﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Utils
{
    public static class Utils
    {
        public static string pegaValorEntre(string entry, string first, string second)
        {
            String St = entry;

            int pFrom = St.IndexOf(first) + first.Length;
            int pTo = St.LastIndexOf(second);

            return St.Substring(pFrom, pTo - pFrom);
        }
    }
}
