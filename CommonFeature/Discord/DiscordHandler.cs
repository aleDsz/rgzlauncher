﻿using CommonFeature.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Discord
{
    public class DiscordHandler
    {
        private string clientID = "641084521978134531";
        public DiscordRpc.EventHandlers handlers;
        public DiscordRpc.RichPresence presence;

        public DiscordHandler(string clientID)
        {
            this.clientID = clientID;
            Initialize();
        }

        /// <summary>
		/// Initialize the RPC.
		/// </summary>
		/// <param name="clientId"></param>
		public void Initialize()
        {
            handlers = new DiscordRpc.EventHandlers();

            handlers.readyCallback = ReadyCallback;
            handlers.disconnectedCallback += DisconnectedCallback;
            handlers.errorCallback += ErrorCallback;

            DiscordRpc.Initialize(clientID, ref handlers, true, null);

            //this.SetStatusBarMessage("Initialized.");
        }

        public void ShutDown()
        {
            DiscordRpc.Shutdown();
        }

        public string FirstLetterToUpper(string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }

        public void UpdatePresenceSimple(string detail, string state, string largeimagekey, string largeimagetext, string smallimagekey = "NONE", string smallimagetext = "NONE")
        {
            presence = new DiscordRpc.RichPresence();

            presence.details = detail;
            presence.state = state;

            presence.largeImageKey = largeimagekey;
            presence.largeImageText = largeimagetext;

            if (!smallimagekey.Equals("NONE"))
                presence.smallImageKey = smallimagekey;

            if (!smallimagetext.Equals("NONE"))
                presence.smallImageText = smallimagetext;

            DiscordRpc.UpdatePresence(ref presence);
        }

        /// <summary>
        /// Convert a DateTime object into a timestamp.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private long DateTimeToTimestamp(DateTime dt)
        {
            return (dt.Ticks - 621355968000000000) / 10000000;
        }

        /// <summary>
		/// Calls ReadyCallback(), DisconnectedCallback(), ErrorCallback().
		/// </summary>
		private void RunCallbacks()
        {
            DiscordRpc.RunCallbacks();

            //this.SetStatusBarMessage("Rallbacks run.");
        }

        /// <summary>
        /// Stop RPC.
        /// </summary>
        private void Shutdown()
        {
            DiscordRpc.Shutdown();

            //this.SetStatusBarMessage("Shuted down.");
        }

        /// <summary>
        /// Called after RunCallbacks() when ready.
        /// </summary>
        private void ReadyCallback()
        {
            //this.SetStatusBarMessage("Ready.");
        }

        /// <summary>
		/// Called after RunCallbacks() in cause of disconnection.
		/// </summary>
		/// <param name="errorCode"></param>
		/// <param name="message"></param>
		private void DisconnectedCallback(int errorCode, string message)
        {
            //this.SetStatusBarMessage(string.Format("Disconnect {0}: {1}", errorCode, message));
        }

        /// <summary>
        /// Called after RunCallbacks() in cause of error.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="message"></param>
        private void ErrorCallback(int errorCode, string message)
        {
            //this.SetStatusBarMessage(string.Format("Error {0}: {1}", errorCode, message));
        }
    }
}
