﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Const
{
    public enum E_Language : int
    {
        English,
        Portuguese,
        Spanish
    }

    public enum E_RESOURCES : int
    {
        DB,
        Monster,
        SFX,
        MasteryImages,
        StickerImages,
        ChatIcons,
        Sound,
    }

    public enum E_INIT : int
    {
        Basic,
        Language,
        Discord,
        Monsters,
        Megumi,
        Patcher,
        Mastery
    }

    public static class Const_Connection
    {
        public static string ip_localhost { get; set; } = "127.0.0.1";
        public static string ip_vps { get; set; } = "170.81.41.203";
        public static int loginPort { get; set; } = 6900;
        public static int charPort { get; set; } = 6121;
        public static int mapPort { get; set; } = 5121;

    }
}
