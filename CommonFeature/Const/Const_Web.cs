﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Const
{
    public static class E_WEB
    {
        public static string SERVER_IP = "127.0.0.1";
        public static int MAP_SERVER = 6969;

        public static string NEWS_URL_BASE = "http://www.ragnaghostz.com.br/Patcher/News/";
        public static string FILES_URL_BASE = "http://www.ragnaghostz.com.br/Patcher/Files/";

        public static string PATCH_FILE = "http://www.ragnaghostz.com.br/Patcher/patch_files.yaml";
        public static string PATCH_NEWS = "http://www.ragnaghostz.com.br/Patcher/patch_news.yaml";
    }
}
