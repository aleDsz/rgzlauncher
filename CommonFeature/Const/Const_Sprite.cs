﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Const
{
    public enum E_SEX
    {
        FEMALE,
        MALE
    }

    public enum E_CHARSPRITE
    {
        HAIR,
        HAIR_COLOR,
        BODY,
        BODY_COLOR
    }
}
