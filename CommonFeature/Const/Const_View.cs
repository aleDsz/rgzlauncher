﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Const
{
    public enum E_VIEW : int
    {
        E_SPLASHSCREEN,
        E_LOGINSCREEN,
        E_MAINHUD,
        E_MASTERYSCREEN,
        E_CHATSCREEN,
    }

    public enum E_GAMEVIEW : int
    {
        AVATAR,
        LIFE,
        NAME,
        GOLD,
        MANA,
        MASTER_LEVEL,
        MATCH_LEVEL,
        MATCH_EXP
    }
}
