﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Const
{
    public enum E_PACKET
    {
        MEGUMI_LOGIN = 2478,
        BUYMONSTER,
        MOUSEDOWN,
        MOUSEUP,
        ROLLMONSTERS,

        //RagnaGhostz
        STYLE = 3000,
        CLOSED_MASTERY,
        BUY_MASTERY,
        HELP_MASTERY,
        REFRESH_MASTERY,
        SEND_MAC,
        TALKING,
        STICKER,
        WHISPER,
    }

    public enum E_CLEARDATA
    {
        MASTERY = 1,
        CHAT = 2,
    }
}
