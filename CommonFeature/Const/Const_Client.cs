﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Const
{
    public enum E_WALKDIR
    {
        BAIXO = 0,
        DIAGONAL_ESQUERDA_BAIXO,
        ESQUERDA,
        DIAGONAL_ESQUERDA_CIMA,
        CIMA,
        DIAGONAL_DIREITA_CIMA,
        DIREITA,
        DIAGONAL_DIREITA_BAIXO,
        NONE = 99,
    }

    public enum E_CAMERADIR
    {
        BAIXO = 0,
        DIAGONAL_ESQUERDA_BAIXO,
        ESQUERDA,
        DIAGONAL_ESQUERDA_CIMA,
        CIMA,
        DIAGONAL_DIREITA_CIMA,
        DIREITA,
        DIAGONAL_DIREITA_BAIXO
    }

    public enum E_CURSOR
    {
        NORMAL,
        CLICK,
        BLOCK
    }
}
