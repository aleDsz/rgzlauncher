﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Client
{
    public static class OffSets
    {
        public static Hook RagHook;

        public struct Player
        {
            public static IntPtr GID { get { return (IntPtr)0x00CD93C8; } }
            public static IntPtr CID { get { return (IntPtr)0x00CD93CC; } }
            public static IntPtr CHAR_NAME { get { return (IntPtr)0x00CDF5D8; } }
            public static IntPtr CHAR_CLASS { get { return (IntPtr)0x00CD93D8; } }
            public static IntPtr CHAR_BASELEVEL { get { return (IntPtr)0x00CD9400; } }
            public static IntPtr CHAR_JOBLEVEL { get { return (IntPtr)0x00CD9408; } }
            public static IntPtr ZENY { get { return (IntPtr)0x00CD94A0; } }
            public static IntPtr SEX { get { return (IntPtr)0x00CDCDB8; } }
            public static IntPtr HP { get { return (IntPtr)0x00CDCE10; } }
            public static IntPtr HP_MAX { get { return (IntPtr)0x00CDCE14; } }
            public static IntPtr SP { get { return (IntPtr)0x00CDCE18; } }
            public static IntPtr SP_MAX { get { return (IntPtr)0x00CDCE1C; } }
            public static IntPtr BASE_EXP { get { return (IntPtr)0x00CD93E0; } }
            public static IntPtr BASE_EXPMAX { get { return (IntPtr)0x00CD93E8; } }
            public static IntPtr JOB_EXP { get { return (IntPtr)0x00CD93F8; } }
            public static IntPtr JOB_EXPMAX { get { return (IntPtr)0x00CD93F0; } }
        };

        public struct HUD
        {
            public static IntPtr BASEINFO_EXPAND_HIDE_SHOW { get { return (IntPtr)0x00CDF4A0; } }
            public static IntPtr BASEINFO_HIDE_SHOW { get { return (IntPtr)0x00B33CE0; } }
            public static IntPtr STATS_HIDE_SHOW { get { return (IntPtr)0x00B34068; } }
            public static IntPtr INVENTORY_HIDE_SHOW { get { return (IntPtr)0x00B3402C; } }
            public static IntPtr EQUIPMENT_HIDE_SHOW { get { return (IntPtr)0x00B34080; } }
            public static IntPtr SKILL_HIDE_SHOW { get { return (IntPtr)0x00B34258; } }
            public static IntPtr QUEST_HIDE_SHOW { get { return (IntPtr)0x00B34048; } }
            public static IntPtr SKILLBAR_HIDE_SHOW { get { return (IntPtr)0x00B33CEC; } }

            public static IntPtr CHAT_HIDE_SHOW { get { return (IntPtr)0x00B33CD0; } }
            public static IntPtr CHAT_DIALOGBOX { get { return (IntPtr)0x00B33CD0; } }
            public static IntPtr CHAT_TEXTBOX_HIDE_SHOW { get { return (IntPtr)0x00B33B48; } }
        }

        public struct STYLE_HUD
        {
            public static IntPtr HAIR_COLOR { get { return (IntPtr)0x00B37398; } }
            public static IntPtr HAIR_STYLE { get { return (IntPtr)0x00B37398; } }
            public static IntPtr CLOTH_COLOR { get { return (IntPtr)0x00B37398; } }
        }

        public struct SCREEN
        {
            public static IntPtr CURSOR_XPOS { get { return (IntPtr)0x00A2EC74; } }
            public static IntPtr CURSOR_GROUNDX { get { return (IntPtr)0x00A623B4; } }
            public static IntPtr CURSOR_GROUNDY { get { return (IntPtr)0x00A623B8; } }

            public static IntPtr CAMERA_ROTATION { get { return (IntPtr)0x00A43F44; } }
            public static IntPtr CAMERA_ZOOM { get { return (IntPtr)0x00A43F44; } }
            public static IntPtr CAMERA_LATITUDE { get { return (IntPtr)0x00A43F44; } }
        }

        public struct MAP
        {
            public static IntPtr NAME { get { return (IntPtr)0x00A43F48; } }
            public static IntPtr X { get { return (IntPtr)0x00CC5CA4; } }
            public static IntPtr Y { get { return (IntPtr)0x00CC5CA8; } }
        }

        public static IntPtr GetBasicInfoOffset(IntPtr ptr)
        {
            IntPtr pointer = RagHook.MS[ptr].Read<IntPtr>();

            IntPtr finalPtr = IntPtr.Zero;

            if (pointer != IntPtr.Zero)
            {
                finalPtr = IntPtr.Add(pointer, 40);
            }

            return finalPtr;
        }

        public static IntPtr GetChatPointer(IntPtr ptr)
        {
            IntPtr pointerChat = RagHook.MS.Read<IntPtr>(ptr);

            IntPtr finalPtr = IntPtr.Zero;

            if (pointerChat != IntPtr.Zero)
                finalPtr = IntPtr.Add(pointerChat, 40);

            return finalPtr;
        }

        // Valor + 40 -> Ponteiro com Valor
        public static IntPtr GetSkillBarOffset(IntPtr ptr)
        {
            IntPtr skillBar = RagHook.MS[ptr].Read<IntPtr>();

            IntPtr finalPtr = IntPtr.Zero;

            if (skillBar != IntPtr.Zero)
                finalPtr = IntPtr.Add(skillBar, 40);

            return finalPtr;
        }

        // Valor + 48 -> Primeiro Ponteiro -> Segundo Ponteiro -> Valor Float

        //Rotação da Camera
        //POINTER = 00E43F44 + 208
        //POINTER = VALUE(POINTER) + 48
        //ROTATION = VALUE(FLOAT)
        //CAMERA Y ANCHOR -> ROTATION POINTER - 8
        //CAMERA X ANCHOR -> ROTATION POINTER - 16
        public static IntPtr GetCameraRotationOffset(IntPtr ptr, string type)
        {
            IntPtr cameraRotation = RagHook.MS[ptr].Read<IntPtr>();

            IntPtr finalPtr = IntPtr.Zero;

            if (cameraRotation != IntPtr.Zero)
            {
                cameraRotation = RagHook.MS[cameraRotation + 208, false].Read<IntPtr>();

                if (cameraRotation != IntPtr.Zero)
                {
                    switch(type)
                    {
                        case "ROTATION":
                            finalPtr = RagHook.MS[cameraRotation + 48, false].BaseAddress;
                            break;

                        case "ZOOM":
                            finalPtr = RagHook.MS[cameraRotation + 76, false].BaseAddress;
                            break;

                        case "LATITUDE":
                            finalPtr = RagHook.MS[cameraRotation + 44, false].BaseAddress;
                            break;
                    }
                }
            }

            return finalPtr;
        }
    }
}
