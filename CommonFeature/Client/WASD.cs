﻿using CommonFeature.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Client
{
    public class XY
    {
        public int x { get; set; } = 0;
        public int y { get; set; } = 0;

        private E_CAMERADIR getCamDir(int rotation)
        {
            if (rotation >= 23 && rotation <= 66)
                return E_CAMERADIR.DIAGONAL_ESQUERDA_BAIXO; // Angulos 23 a 66

            if (rotation >= 67 && rotation <= 112)
                return E_CAMERADIR.ESQUERDA; // Angulos 67 a 120

            if (rotation >= 113 && rotation <= 157)
                return E_CAMERADIR.DIAGONAL_ESQUERDA_CIMA; // Angulos 113 a 157

            if (rotation >= 158 && rotation <= 202)
                return E_CAMERADIR.CIMA; // Angulos 158 a 202

            if (rotation >= 203 && rotation <= 247)
                return E_CAMERADIR.DIAGONAL_DIREITA_CIMA; // Angulos 203 a 247

            if (rotation >= 248 && rotation <= 292)
                return E_CAMERADIR.DIREITA; // Angulos 248 a 292

            if (rotation >= 293 && rotation <= 338)
                return E_CAMERADIR.DIAGONAL_DIREITA_BAIXO; // Angulos 293 a 338

            return E_CAMERADIR.BAIXO; // Angulos 339 a 360/23
        }

        public XY walkLogic(E_WALKDIR dir, int rotation)
        {
            E_CAMERADIR cam = getCamDir(rotation);

            List<XY> xyList = new List<XY>();

            xyList.Add(new XY() { y = -1 });
            xyList.Add(new XY() { x = -1, y = -1 });
            xyList.Add(new XY() { x = -1 });
            xyList.Add(new XY() { x = -1, y = 1 });
            xyList.Add(new XY() { y = 1 });
            xyList.Add(new XY() { x = 1, y = 1 });
            xyList.Add(new XY() { x = 1 });
            xyList.Add(new XY() { x = 1, y = -1 });

            int dir_ = (int)dir;

            for (int i = 0; i < dir_; i++)
                xyList.Add(xyList[i]);

            return xyList[(int)cam + (int)dir];
        }
    }

    public class KeyLogic
    {
        public bool W_IsDown { get; set; } = false;
        public bool A_IsDown { get; set; } = false;
        public bool S_IsDown { get; set; } = false;
        public bool D_IsDown { get; set; } = false;
        public int totalHold { get; set; } = 0;
        public E_WALKDIR lastDir = E_WALKDIR.NONE;

        public bool isAllUp()
        {
            return (!W_IsDown && !A_IsDown && !S_IsDown && !D_IsDown);
        }
    }
}
