﻿using CommonFeature.Objects;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CommonFeature.Client
{
    public class Ragexe
    {
        public Process Game;
        public int Client_Height;
        public int Client_Widht;
        public string ExecutableName;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, UInt32 uFlags);

        public Ragexe(string executable_name, User user)
        {
            ExecutableName = executable_name;

            startClientAsync(user.Username, user.Password);
        }

        public bool IsRunning()
        {
            if (Game == null)
                return false;
            try
            {
                Process.GetProcessById(Game.Id);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool IsFocused()
        {
            IntPtr hwnd = WinApi.GetForegroundWindow();

            if (hwnd != Game.MainWindowHandle)
                return false;

            return true;
        }

        private async Task startClientAsync(string username, string password)
        {
            string argsStart = string.Format("-t:{0} {1} 1rag1", password, username.ToLower());

            Game = new Process();

            Game.StartInfo.FileName = Directory.GetCurrentDirectory() + ExecutableName;

            Game.StartInfo.Arguments = argsStart;
            Game.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            Game.Start();

            //if (SS.ckFullScreen.IsChecked.Value) TODO: Depois descomentar
            //{
            //SetWindowPos(LS.Ragexe.MainWindowHandle, HWND_TOPMOST, 0, 0, (int)SystemParameters.PrimaryScreenWidth,
            //(int)SystemParameters.PrimaryScreenHeight, SWP_SHOWWINDOW);

            while (string.IsNullOrEmpty(Game.MainWindowTitle))
            {
                System.Threading.Thread.Sleep(150);
                Game.Refresh();
            }
        }

        //Gets window attributes
        [DllImport("USER32.DLL")]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        //Sets window attributes
        [DllImport("USER32.DLL")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        public static int GWL_STYLE = -16;
        public static int WS_BORDER = 0x00800000; //window with border
        public static int WS_DLGFRAME = 0x00400000; //window with double border but no title
        public static int WS_CAPTION = WS_BORDER | WS_DLGFRAME; //window with a title bar
        private const int WS_SYSMENU = 0x00080000;      //window with no borders etc.
        private const int WS_MAXIMIZEBOX = 0x00010000;
        private const int WS_MINIMIZEBOX = 0x00020000;  //window with minimizebox

        public static void HideWindowBorders(IntPtr hWnd)
        {
            int style = GetWindowLong(hWnd, GWL_STYLE); //gets current style
            SetWindowLong(hWnd, GWL_STYLE, (style & ~WS_CAPTION)); //removes caption from current style
        }
    }

    public static class ProcessExtends
    {
        public static Task WaitForExitAsync(this Process process, CancellationToken cancellationToken = default(CancellationToken))
        {
            var tcs = new TaskCompletionSource<object>();
            process.EnableRaisingEvents = true;
            process.Exited += (sender, args) => tcs.TrySetResult(null);
            if (cancellationToken != default(CancellationToken))
                cancellationToken.Register(tcs.SetCanceled);

            return tcs.Task;
        }
    }
}
