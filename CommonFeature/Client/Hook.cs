﻿using Binarysharp.MemoryManagement;
using CommonFeature.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Client
{
    public class Hook
    {
        public MemorySharp MS;

        public Hook(Ragexe rag)
        {
            MS = new MemorySharp(rag.Game);

            OffSets.RagHook = this;
        }

        public int GetSelectedInStyleUI(E_CHARSPRITE type)
        {
            IntPtr pointer = MS.Read<IntPtr>(OffSets.STYLE_HUD.HAIR_COLOR);

            switch (type)
            {
                case E_CHARSPRITE.HAIR_COLOR:
                    {
                        IntPtr hair_color;

                        if (pointer != IntPtr.Zero)
                        {
                            hair_color = IntPtr.Add(pointer, 160);

                            return MS.Read<int>(hair_color, false);
                        }
                    }
                    break;

                case E_CHARSPRITE.HAIR:
                    {
                        IntPtr hair_style;

                        if (pointer != IntPtr.Zero)
                        {
                            hair_style = IntPtr.Add(pointer, 164);

                            return MS.Read<int>(hair_style, false);
                        }
                    }
                    break;

                case E_CHARSPRITE.BODY_COLOR:
                    {
                        IntPtr cloth_color;

                        if (pointer != IntPtr.Zero)
                        {
                            cloth_color = IntPtr.Add(pointer, 168);

                            return MS.Read<int>(cloth_color, false);
                        }
                    }
                    break;
            }

            return 0;
        }

        public void HideRagHUD()
        {
            try
            {
                IntPtr pointer;
                //
                ////BasicInfo
                //IntPtr basicinfo = MS.Read<IntPtr>(OffSets.HUD.BASEINFO_HIDE_SHOW);
                //
                //if (basicinfo != IntPtr.Zero)
                //{
                //    MS.Write<byte>(IntPtr.Add(basicinfo, 40), 0, false);
                //
                //    int action = MS.Read<int>(OffSets.HUD.BASEINFO_EXPAND_HIDE_SHOW);
                //
                //    if (action > 0)
                //        MS.Write<byte>((IntPtr)OffSets.HUD.BASEINFO_EXPAND_HIDE_SHOW, Convert.ToByte(0));
                //}

                // Chat
                // Valor + 40 -> Ponteiro com Valor
                IntPtr chat = MS.Read<IntPtr>(OffSets.HUD.CHAT_HIDE_SHOW);

                if (chat != IntPtr.Zero)
                {
                    pointer = IntPtr.Add(chat, 40);
                    MS.Write<byte>(pointer, 0, false);
                }

                chat = MS.Read<IntPtr>(OffSets.HUD.CHAT_DIALOGBOX);

                if(chat != IntPtr.Zero)
                {
                    pointer = IntPtr.Add(chat, 28);
                    MS.Write<int>(pointer, 9999, false);

                    pointer = IntPtr.Add(pointer, 136);
                    MS.Write<int>(pointer, 9999, false);
                }

                chat = MS.Read<IntPtr>(OffSets.HUD.CHAT_TEXTBOX_HIDE_SHOW);

                if( chat != IntPtr.Zero)
                {
                    MS.Write<byte>(chat, 0, true);
                }

                //Skill Bar
                // Valor + 40 -> Ponteiro com Valor
                //IntPtr skill = MS[OffSets.HUD.SKILLBAR_HIDE_SHOW].Read<IntPtr>();
                //
                //if (skill != IntPtr.Zero)
                //{
                //    pointer = IntPtr.Add(skill, 40);
                //    MS.Write<byte>(pointer, 0, false);
                //}

            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void MoveCursor(int x, int y)
        {
            MS.Write<int>(OffSets.SCREEN.CURSOR_XPOS, x, true);
        }

        public UInt16[] GetGroundCursorXY()
        {
            UInt16[] xy = { 0, 0 };

            xy[0] = (UInt16)GetIntValue(OffSets.SCREEN.CURSOR_GROUNDX, true);
            xy[1] = (UInt16)GetIntValue(OffSets.SCREEN.CURSOR_GROUNDY, true);

            return xy;
        }

        public int GetIntValue(IntPtr ptr, bool isRelative)
        {
            return MS.Read<int>(ptr, isRelative);
        }

        public string GetStringValue(IntPtr ptr)
        {
            return MS.ReadString(ptr, true);
        }

        public byte GetByteValue(IntPtr ptr, bool isRelative)
        {
            return MS.Read<byte>(ptr, isRelative);
        }

        public float GetFloatValue(IntPtr ptr, bool isRelative)
        {
            return MS.Read<float>(ptr, isRelative);
        }
    }
}
