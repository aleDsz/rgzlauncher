﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonFeature.Const;
using ProgressReporting;
using YamlDotNet.Core;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;

namespace CommonFeature.Translator
{
    public sealed class TranslatorSingleton
    {
        static TranslatorSingleton _instance;

        private Dictionary<string, Dictionary<string, string>> Dic;

        E_Language user_language = E_Language.English;
            
        public static TranslatorSingleton Instance
        {
            get { return _instance ?? (_instance = new TranslatorSingleton()); }
        }

        private TranslatorSingleton()
        {
            Dic = new Dictionary<string, Dictionary<string, string>>();
        }

        public void SetUserLanguage(string directory, E_Language language)
        {
            user_language = language;

            Dic.Clear();

            LoadLanguageText(directory);
        }

        public string GetText(string session, string key)
        {
            Dictionary<string, string> node;
            String text = "";

            Dic.TryGetValue(session, out node);
            node.TryGetValue(key, out text);

            return text == "" ? "Error loading text" : text;
        }

        private void LoadLanguageText(string dbpath)
        {
            return;

            string filename = "";

            switch (user_language)
            {
                default:
                case E_Language.English:
                    filename = "eng";
                    break;

                case E_Language.Portuguese:
                    filename = "ptbr";
                    break;
            }

            TextReader db = new StringReader(File.ReadAllText(dbpath + "msgstringtable.yml", Encoding.UTF8));

            var deserializer = new DeserializerBuilder().Build();

            var parser = new Parser(db);

            parser.Expect<StreamStart>();

            while (parser.Accept<DocumentStart>())
            {
                var doc = deserializer.Deserialize<List<Session>>(parser);

                foreach (var item in doc)
                {
                    Dictionary<string, string> node = new Dictionary<string, string>();

                    foreach(var k in item.text)
                    {
                        string key = k.key;
                        string text = "";

                        switch(user_language)
                        {
                            case E_Language.English:
                                text = k.eng;
                                break;

                            case E_Language.Portuguese:
                                text = k.ptbr;
                                break;
                        }

                        node.Add(key, text);
                    }

                    Dic.Add(item.session, node);
                }
            }
        }

        private class Session
        {
            public string session { get; set; }
            public List<Text> text { get; set; } = new List<Text>();
        }
        
        private class Key
        {
            public List<Key> text { get; set; } = new List<Key>();
        }

        private class Text
        {
            public string key { get; set; }
            public string eng { get; set; }
            public string ptbr { get; set; }
        }
    }
}
