﻿using CommonFeature.Const;
using CommonFeature.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Match
{
    public class Match
    {
        public int ID;
        public int Phase;
        public int Turn;

        public int account_id;

        public Dictionary<int, MatchData> MD = new Dictionary<int, MatchData>();
        public MatchPhase[] Phases;

        public MatchData MyMD()
        {
            return MD[account_id];
        }
    }

    public class MatchData
    {
        public int Account_id;

        public VisualData VD = new VisualData();

        public int level;
        public int exp;

        public int Life;
        public int Gold;
        public int Mana;

        public int[] MonsterCards;

        public class VisualData
        {
            public string Name;
            public E_SEX Sex;
            public int Body;
            public int BodyColor;
            public int Hair;
            public int Hair_color;
        }
    }

    public class MatchPhase
    {
        public int Num;
        public int TimeRemaining;

        public Turn[] Turns;
    }

    public class Turn
    {
        public E_TURN Type;
        public int Num;
    }
}
