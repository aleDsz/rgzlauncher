﻿using ProgressReporting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Launcher.Model.Splash
{
    public class ModelSplash : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private TransferProgress _tp = new TransferProgress();

        public TransferProgress TP { get => _tp; set { _tp = value; RaisePropertyChanged(); } }

        private double _tpPercent;

        public double TPPercent { get => (TP.RemainingPercent - TP.CompletedPercent); set { _tpPercent = (TP.RemainingPercent - TP.CompletedPercent); } }

        private void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
