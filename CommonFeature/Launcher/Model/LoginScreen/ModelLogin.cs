﻿using CommonFeature.Const;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using YamlDotNet.Core;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;

namespace Launcher.Model.LoginScreen
{
    public class ModelLogin : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public List<New> News = new List<New>();

        public async void GetNews(Window LS)
        {
            string yaml = await (new WebClient()).DownloadStringTaskAsync(new Uri(E_WEB.PATCH_NEWS));

            TextReader db = new StringReader(yaml);

            var deserializer = new DeserializerBuilder().Build();

            var parser = new Parser(db);

            parser.Expect<StreamStart>();

            while (parser.Accept<DocumentStart>())
            {
                var doc = deserializer.Deserialize<List<New>>(parser);

                foreach (var item in doc)
                {
                    News.Add(item);
                }
            }

            if (News.Count == 0) return;

            if(LS is View.LoginScreen.LoginScreen)
                ((View.LoginScreen.LoginScreen)LS).DisplayNew(News[0].FOLDER, new List<NewsData> { News[0].MAIN, News[0].SKIN, News[0].EVENT, News[0].CARD, News[0].OFFTOPIC, News[0].COMMUNITY });
            else
                ((View.LoginScreen.LoginScreenSmall)LS).DisplayNew(News[0].FOLDER, new List<NewsData> { News[0].MAIN, News[0].SKIN, News[0].EVENT, News[0].CARD, News[0].OFFTOPIC, News[0].COMMUNITY });
        }

        private void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class New
    {
        public string FOLDER { get; set; } = "";

        public NewsData MAIN { get; set; }
        public NewsData SKIN { get; set; }
        public NewsData EVENT { get; set; }
        public NewsData CARD { get; set; }
        public NewsData OFFTOPIC { get; set; }
        public NewsData COMMUNITY { get; set; }
    }

    public class NewsData
    {
        public string IMG { get; set; } = "";
        public string URL { get; set; } = "";
        public bool CALLFUNC { get; set; } = false;
        public string FUNCPARAMS { get; set; } = "";
        public List<NewsText> NEWS { get; set; } = new List<NewsText>();  
    }

    public class NewsText
    {
        public string title { get; set; } = "Lorem Ipsum";
        public string desc { get; set; } = "Lorem Ipsum É O CARALHO";
    }
}
