﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Model.Preferences
{
    public static class UserPreferences
    {
        public static bool Video_AutoPlay { get; set; }
        public static bool Music_AutoPlay { get; set; }
        public static bool RememberCredentials { get; set; }
    }
}
