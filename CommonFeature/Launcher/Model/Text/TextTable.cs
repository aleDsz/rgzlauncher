﻿using CommonFeature.Translator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Launcher.Model.Text
{
    public class TextTable : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(parameter == null || DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                return null;

            string[] str = ((string)parameter).Split('.');

            return TranslatorSingleton.Instance.GetText(str[0], str[1]);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
