﻿using CommonFeature.Const;
using CommonFeature.Objects;
using Launcher.Model.DB;
using Launcher.Model.Packet;
using Launcher.Model.Updater;
using Launcher.Properties;
using Launcher.View;
using Launcher.View.LoginScreen;
using Launcher.View.Main;
using ProgressReporting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;

namespace Launcher.Model
{
    public sealed class CoreSingleton
    {
        static CoreSingleton _instance;

        public Dictionary<E_VIEW, Window> Views = new Dictionary<E_VIEW, Window>();
        public Dictionary<E_VIEW, HookWindow> GameViews = new Dictionary<E_VIEW, HookWindow>();

        public Patcher Patch;
        public CommonFeature.Discord.DiscordHandler Discord;
        public CommonFeature.Client.Ragexe Client;
        public CommonFeature.Client.Hook RagHook;
        public Model.Keyboard.MouseEvents MouseEvents;

        public Megumi.Megumi MegumiControl;
        public ModelPacket MVPacket;

        public Dictionary<E_RESOURCES, string> ResourcesPath;

        public Popup PopUp_Ult;

        //DB
        public DBMonster dbMonster;

        //Timers
        public Thread PacketThread;

        public static CoreSingleton Instance
        {
            get { return _instance ?? (_instance = new CoreSingleton()); }
        }

        private CoreSingleton()
        {

        }

        public void Init(E_INIT init, int[] parameters, TransferProgress TP)
        {
            switch(init)
            {
                case E_INIT.Basic:
                    InitBasic(Directory.GetCurrentDirectory());
                    break;

                case E_INIT.Language:
                    CommonFeature.Translator.TranslatorSingleton.Instance.SetUserLanguage(ResourcesPath[E_RESOURCES.DB], (E_Language)parameters[0]);
                    break;

                case E_INIT.Discord:
                    Discord = new CommonFeature.Discord.DiscordHandler("622901700683890688");
                    break;

                case E_INIT.Monsters:
                    dbMonster = new DBMonster(ResourcesPath[E_RESOURCES.DB], TP);
                    break;

                case E_INIT.Megumi:
                    MVPacket = new ModelPacket();
                    break;

                case E_INIT.Patcher:
                    Patch = new Patcher();
                    break;
            }
        }

        public void InitBasic(string application_dir)
        {
            ResourcesPath = new Dictionary<E_RESOURCES, string>();
            ResourcesPath.Add(E_RESOURCES.DB, application_dir + @"\Resources\Data\DB\");
            ResourcesPath.Add(E_RESOURCES.Monster, application_dir + @"\Resources\Data\Monsters\");
            ResourcesPath.Add(E_RESOURCES.SFX, application_dir + @"\Resources\SFX\");
        }

        public void ReadPacketStack()
        {
            while (MegumiControl.GetPacketQueue().Count > 0)
            {
                string packet = "";

                if (MegumiControl.GetPacketQueue().TryDequeue(out packet))
                {
                    MVPacket.ParserPacket(packet);
                }
            } 
        }

        public void StartGame(string executable, string username, string password)
        {
            MegumiControl = new Megumi.Megumi(username, password);

            PacketThread = new Thread(() =>
            {
                while(true)
                {
                    ReadPacketStack();
                }
            });

            Client = new CommonFeature.Client.Ragexe(executable, MegumiControl.GetSession());
            RagHook = new CommonFeature.Client.Hook(Client);

            ((View.LoginScreen.LoginScreen)Views[E_VIEW.E_LOGINSCREEN]).HideScreen();

            while(!Client.IsRunning())
            {
                Thread.Sleep(100);
            }

            HookWindow HW = new MainHud();
            HW.Show();
            HW.PositionTimer();

            MouseEvents = new Keyboard.MouseEvents();

            PacketThread.Start();
        }

        public void Finish()
        {
            if (Discord != null)
                Discord.ShutDown();

            if (Client != null)
                Client.Game.Kill();

            if (MouseEvents != null)
                MouseEvents.MouseHook.Dispose();

            Settings.Default.Save();

            Environment.Exit(0);
        }
    }
}
