﻿using CommonFeature.Objects;
using CommonFeature.Translator;
using ProgressReporting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Core;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;

namespace Launcher.Model.DB
{
    public class DBMonster
    {
        public Dictionary<int, Monster> Monsters = new Dictionary<int, Monster>();

        public DBMonster(string dbpath, TransferProgress TP)
        {
            return;

            TextReader db = new StringReader(File.ReadAllText(dbpath + "mob.yml", Encoding.UTF8));

            var deserializer = new DeserializerBuilder().Build();

            var parser = new Parser(db);

            parser.Expect<StreamStart>();

            TP.Restart(50);

            while (parser.Accept<DocumentStart>())
            {
                var doc = deserializer.Deserialize<List<MonsterYML>>(parser);

                int c = 0;

                foreach (var item in doc)
                {
                    Monster mob = new Monster()
                    {
                        Id = item.mob_id,
                        Level = item.level,
                        Atk_range = item.attack_range,
                        Life = item.hp,
                        Life_max = item.hp,
                        Mana_Start = item.sp_start,
                        Mana_max = item.sp_end,
                        Atk_min = item.attack_min,
                        Atk_max = item.attack_max,
                        Def_atk = item.defense_attack + item.dex,
                        Def_magic = item.defense_magic,
                        Move_speed = item.speed,
                        Atk_delay = item.attack_delay,
                        Atk_critical = (short)item.luk,
                        Cost = (short)item.cost,
                    };

                    string key = string.Format("{0}_", mob.Id);

                    mob.Name = TranslatorSingleton.Instance.GetText("MONSTERS", key + "NAME");
                    mob.History = TranslatorSingleton.Instance.GetText("MONSTERS", key + "HISTORY");
                    mob.Ultimate_desc = TranslatorSingleton.Instance.GetText("MONSTERS", key + "ULT");
                    mob.Voiceactor = TranslatorSingleton.Instance.GetText("MONSTERS", key + "DUBBER");

                    Monsters.Add(mob.Id, mob);
                }
            }
        }
    }

    public class MonsterYML
    {
        public int mob_id { get; set; }
        public int level { get; set; }
        public int str { get; set; }
        public int agi { get; set; }
        public int vit { get; set; }
        public int int_ { get; set; }
        public int dex { get; set; }
        public int luk { get; set; }
        public int attack_range { get; set; }
        public int hp { get; set; }
        public int sp_start { get; set; }
        public int sp_end { get; set; }
        public int attack_min { get; set; }
        public int attack_max { get; set; }
        public int defense_attack { get; set; }
        public int defense_magic { get; set; }
        public int speed { get; set; }
        public int attack_motion { get; set; }
        public int attack_delay { get; set; }
        public int id { get; set; }
        public int extra_id { get; set; }
        public string nome { get; set; }
        public string race { get; set; }
        public string class_ { get; set; }
        public string class2 { get; set; }
        public string element { get; set; }
        public int hitanimation { get; set; }
        public int skill { get; set; }
        public int cost { get; set; }
        public int sound { get; set; }
    }

}
