﻿using CommonFeature.Const;
using CommonFeature.Match;
using CommonFeature.Objects;
using Launcher.UC.MonsterCard;
using Launcher.View.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Launcher.Model.Packet
{
    public static class DispatcherUtil
    {

        public static void AsyncWorkAndUIThreadUpdate<T>(this Dispatcher currentDispatcher, Func<T> threadWork, Action<T> guiUpdate)
        {
            ThreadPool.QueueUserWorkItem(delegate (object state)
            {
                T resultAfterThreadWork = threadWork();
                currentDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<T>(delegate (T result) {
                    guiUpdate(resultAfterThreadWork);
                }), resultAfterThreadWork);

            });
        }

    }

    public class ModelPacket
    {
        public void ParserPacket(string packet)
        {
            string[] pk = packet.Split(':');

            if (!pk[0].Equals("CMD")) return;

            string[] data = pk[2].Split('|');
            int[] extra_int = new int[8];
            
            switch(pk[1])
            {
                case "DRESSROOM":
                    MessageBox.Show("Aberto a DressRoom! " + Int32.Parse(data[0]));
                    break;
            }
        }
    }
}
