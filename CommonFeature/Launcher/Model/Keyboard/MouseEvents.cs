﻿using Gma.System.MouseKeyHook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Launcher.Model.Keyboard
{
    public class MouseEvents
    {
        public IKeyboardMouseEvents MouseHook;

        private bool MouseIsDown = true;

        private Thread MouseDownThread;

        public MouseEvents()
        {
            MouseHook = Hook.GlobalEvents();

            MouseHook.MouseDown += OnMouseDown;
            MouseHook.MouseUp += OnMouseUp;
        }

        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) return;

            MouseIsDown = false;

            if(MouseDownThread != null)
                MouseDownThread.Abort();

            CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_SIMPLE(CommonFeature.Const.E_PACKET.MOUSEUP);
        }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (!CoreSingleton.Instance.Client.IsFocused() ||
                e.Button == MouseButtons.Right ||
                CoreSingleton.Instance.MegumiControl.GetSession().SelfData == null ||
                CoreSingleton.Instance.MegumiControl.GetSession().SelfData.PlayerMatch == null ||
                CoreSingleton.Instance.MegumiControl.GetSession().SelfData.PlayerMatch.ID <= 0) return;

            UInt16[] xy = CoreSingleton.Instance.RagHook.GetGroundCursorXY();

            //TODO: Só enviar este packet se o jogador estiver em um mapa de arena
            CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_MOUSEDOW(xy[0], xy[1]);
            MouseIsDown = true;

            MouseDownThread = new Thread(() =>
            {
                xy = CoreSingleton.Instance.RagHook.GetGroundCursorXY();

                while (MouseIsDown && CoreSingleton.Instance.Client.IsFocused())
                {
                    UInt16[] xyNew = CoreSingleton.Instance.RagHook.GetGroundCursorXY();

                    if (xy[0] != xyNew[0] || xy[1] != xyNew[1] )
                    {
                        xy = xyNew;
                        CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_MOUSEDOW(xy[0], xy[1]);
                    }

                    Thread.Sleep(150);
                }
            });

            MouseDownThread.Start();
        }
    }
}
