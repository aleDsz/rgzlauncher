﻿#pragma checksum "..\..\..\..\..\UC\Monster\MonsterCard.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2B67CFE4AD5B34DAC44BB694227C090982165CCD"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Launcher.Components.Text;
using Launcher.UC.MonsterCard;
using Launcher.ViewModel.UC.Monster;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using SourceChord.GridExtra;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfAnimatedGif;


namespace Launcher.UC.MonsterCard {
    
    
    /// <summary>
    /// MonsterCard
    /// </summary>
    public partial class MonsterCard : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.UC.MonsterCard.MonsterCard MCUC;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid MainGrid;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.ImageBrush splash;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valMonsterLife;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valMonsterMana;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valArmor;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valMagicResist;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valAtk;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valAtkDelay;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valAtkRange;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valCriticChance;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valWalkSpeed;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btRoll;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valElements;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valRaces;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valClass;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.ImageBrush CostBG;
        
        #line default
        #line hidden
        
        
        #line 155 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock valCost;
        
        #line default
        #line hidden
        
        
        #line 168 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Launcher.Components.Text.BeautyTextBlock lbMonsterName;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Launcher;component/uc/monster/monstercard.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.MCUC = ((Launcher.UC.MonsterCard.MonsterCard)(target));
            
            #line 13 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
            this.MCUC.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.UserControl_MouseDown);
            
            #line default
            #line hidden
            
            #line 13 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
            this.MCUC.MouseEnter += new System.Windows.Input.MouseEventHandler(this.MainGrid_MouseEnter);
            
            #line default
            #line hidden
            
            #line 13 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
            this.MCUC.MouseLeave += new System.Windows.Input.MouseEventHandler(this.MainGrid_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 2:
            this.MainGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.splash = ((System.Windows.Media.ImageBrush)(target));
            return;
            case 4:
            this.valMonsterLife = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 5:
            this.valMonsterMana = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 6:
            this.valArmor = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 7:
            this.valMagicResist = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 8:
            this.valAtk = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 9:
            this.valAtkDelay = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 10:
            this.valAtkRange = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 11:
            this.valCriticChance = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 12:
            this.valWalkSpeed = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 13:
            this.btRoll = ((System.Windows.Controls.Button)(target));
            
            #line 111 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
            this.btRoll.MouseEnter += new System.Windows.Input.MouseEventHandler(this.MainGrid_MouseRightButtonDown);
            
            #line default
            #line hidden
            
            #line 111 "..\..\..\..\..\UC\Monster\MonsterCard.xaml"
            this.btRoll.MouseLeave += new System.Windows.Input.MouseEventHandler(this.MainGrid_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 14:
            this.valElements = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 15:
            this.valRaces = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 16:
            this.valClass = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 17:
            this.CostBG = ((System.Windows.Media.ImageBrush)(target));
            return;
            case 18:
            this.valCost = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            case 19:
            this.lbMonsterName = ((Launcher.Components.Text.BeautyTextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

