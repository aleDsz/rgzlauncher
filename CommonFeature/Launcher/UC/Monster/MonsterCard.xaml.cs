﻿using CommonFeature.Objects;
using CommonFeature.Translator;
using ExtendedRichTextBoxLibNuGet;
using Launcher.Model;
using Launcher.Model.Text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;

namespace Launcher.UC.MonsterCard
{
    /// <summary>
    /// Interaction logic for MonsterCard.xaml
    /// </summary>
    public partial class MonsterCard : UserControl
    {
        public Monster monster;
        private TextTable TT = new TextTable();

        public MonsterCard()
        {
            InitializeComponent();

            Visibility = Visibility.Hidden;
            //if (CoreSingleton.Instance.dbMonster.Monsters != null)
            //{
            //    foreach (Monster value in RandomValues(CoreSingleton.Instance.dbMonster.Monsters).Take(1))
            //        MonsterToView(value);
            //}
        }

        public IEnumerable<TValue> RandomValues<TKey, TValue>(IDictionary<TKey, TValue> dict)
        {
            Random rand = new Random();
            List<TValue> values = Enumerable.ToList(dict.Values);
            int size = dict.Count;
            while (true)
            {
                yield return values[rand.Next(size)];
            }
        }

        public MonsterCard(Monster monster)
        {
            this.DataContext = monster;
            this.monster = monster;
            InitializeComponent();

            CommonFeature.Translator.TranslatorSingleton T = TranslatorSingleton.Instance;

            valArmor.ToolTip = new ToolTip() { Content = T.GetText("MONSTERCARD", "DEFATK") };
            valAtk.ToolTip = new ToolTip() { Content = T.GetText("MONSTERCARD", "ATK") };
            valAtkDelay.ToolTip = new ToolTip() { Content = T.GetText("MONSTERCARD", "ADELAY") };
            valCriticChance.ToolTip = new ToolTip() { Content = T.GetText("MONSTERCARD", "CRITICAL") };
            valMagicResist.ToolTip = new ToolTip() { Content = T.GetText("MONSTERCARD", "DEFATK") };
            valMonsterLife.ToolTip = new ToolTip() { Content = T.GetText("MONSTERCARD", "LIFE") };
            valMonsterMana.ToolTip = new ToolTip() { Content = T.GetText("MONSTERCARD", "SP") };
            valWalkSpeed.ToolTip = new ToolTip() { Content = T.GetText("MONSTERCARD", "SPEEDLOCOMOTION") };

            var splashart = new BitmapImage();
            splashart.BeginInit();
            splashart.UriSource = new Uri(CoreSingleton.Instance.ResourcesPath[CommonFeature.Const.E_RESOURCES.Monster] + @"Splash\" + monster.Id + ".png");
            splashart.EndInit();
            splash.ImageSource = splashart;

            UltDesc.Child = monster.CloneRTBDesc();
            UltDesc.Placement = PlacementMode.Top;
            UltDesc.PlacementTarget = this;
            UltDesc.Opacity = 0.80;
            UltDesc.AllowsTransparency = true;
        }

        Popup UltDesc = new Popup();

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Right ||
                CoreSingleton.Instance.MegumiControl.GetSession().SelfData.PlayerMatch == null ||
                CoreSingleton.Instance.MegumiControl.GetSession().SelfData.PlayerMatch.MyMD().Gold < monster.Cost)
                return;

            Visibility = Visibility.Hidden;
            CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_BUYMONSTER(monster.Id);
        }

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hwnd);

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private void MainGrid_MouseEnter(object sender, MouseEventArgs e)
        {
            SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameViews[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);
        }

        private void MainGrid_MouseLeave(object sender, MouseEventArgs e)
        {
            if (CoreSingleton.Instance.PopUp_Ult != null)
                CoreSingleton.Instance.PopUp_Ult.IsOpen = false;

            SetForegroundWindow(CoreSingleton.Instance.Client.Game.MainWindowHandle);
        }

        private void MainGrid_MouseRightButtonDown(object sender, MouseEventArgs e)
        {
            if (CoreSingleton.Instance.PopUp_Ult != null)
                CoreSingleton.Instance.PopUp_Ult.IsOpen = false;

            CoreSingleton.Instance.PopUp_Ult = UltDesc;
            CoreSingleton.Instance.PopUp_Ult.IsOpen = true;
        }
    }
}
