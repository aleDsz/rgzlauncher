﻿using Launcher.Model.Splash;
using Launcher.Model.Text;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Launcher.ViewModel.Splash
{
    class SplashVM : ViewModelBase
    {
        public ModelSplash Model_Splash;

        public ICommand TransProg { get; set; }

        public SplashVM()
        {
            this.Model_Splash = new ModelSplash();
        }
    }
}
