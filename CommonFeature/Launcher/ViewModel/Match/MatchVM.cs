﻿using Launcher.Model.Text;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.ViewModel.Match
{
    public class MatchVM : ViewModelBase
    {
        public MatchVM()
        {
            this.__fGText = new ObservableCollection<TextTable>();
        }

        private ObservableCollection<TextTable> __fGText;
    }
}
