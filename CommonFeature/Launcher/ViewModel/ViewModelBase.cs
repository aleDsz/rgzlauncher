﻿using Launcher.Model.Text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.ViewModel
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        protected void OnPropertyChanged(string p)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(p));
            }
        }

        private TextTable __tt = new TextTable();

        public TextTable TT
        {
            get { return __tt; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
