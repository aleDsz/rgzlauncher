﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CommonFeature.Client;
using CommonFeature.Const;
using Launcher.Model;

namespace Launcher.View.Main
{
    /// <summary>
    /// Interaction logic for MainHud.xaml
    /// </summary>
    public partial class MainHud : HookWindow
    {
        public MainHud()
        {
            MakeView(E_VIEW.E_MAINHUD);
            InitializeComponent();
        }

        public override void RectLogic(RECT rect)
        {
            MoveWindow(CoreSingleton.Instance.Client.Game.MainWindowHandle, (int)Left, (int)Top + 25, rect.right - rect.left, rect.bottom - rect.top, true);
        }

        private void BtClose_MouseEnter(object sender, MouseEventArgs e)
        {
            btClose.Source = new BitmapImage(new Uri(@"pack://application:,,,/Resources/HUD/CLOSE_OVER.png", UriKind.Absolute));

        }

        private void BtClose_MouseLeave(object sender, MouseEventArgs e)
        {
            btClose.Source = new BitmapImage(new Uri(@"pack://application:,,,/Resources/HUD/CLOSE.png", UriKind.Absolute));
        }

        private void BtClose_MouseUp(object sender, MouseButtonEventArgs e)
        {
            CoreSingleton.Instance.Finish();
        }

        public override void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.DragMove();
            }
            catch (Exception ex)
            {

            }
        }

        public override void Window_LocationChanged(object sender, EventArgs e)
        {
            RECT Rect = new RECT();

            var get = WinApi.GetWindowRect(CoreSingleton.Instance.Client.Game.MainWindowHandle, out Rect);

            foreach (KeyValuePair<E_VIEW, HookWindow> view in CoreSingleton.Instance.GameViews)
            {
                view.Value?.RectLogic(Rect);
            }
        }
    }
}
