﻿using CommonFeature.Match;
using CommonFeature.Translator;
using Launcher.Model;
using Launcher.Model.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Launcher.View.Match
{
    /// <summary>
    /// Interaction logic for PlayerHand.xaml
    /// </summary>
    public partial class PlayerHand : UserControl
    {
        public PlayerHand()
        {
            InitializeComponent();
        }

        public void UpdateSelfData(MatchData player)
        {
            valTotalGold.Text = player.Gold.ToString();
            valTotalMana.Text = player.Mana.ToString();

            int[] exp_table = new int[8] { 2, 2, 6, 12, 20, 32, 50, 70 };

            btExp.ToolTip = TranslatorSingleton.Instance.GetText("PLAYERHAND", "BUYEXP").Replace("EXPACTUAL", player.exp.ToString()).Replace("EXPMAX", exp_table[player.level].ToString());
        }

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hwnd);

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private void MouseEnterBehavior(object sender, MouseEventArgs e)
        {
            SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameViews[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);
        }

        private void MouseLeaveBehavior(object sender, MouseEventArgs e)
        {
            if (CoreSingleton.Instance.PopUp_Ult != null)
                CoreSingleton.Instance.PopUp_Ult.IsOpen = false;

            SetForegroundWindow(CoreSingleton.Instance.Client.Game.MainWindowHandle);
        }

        private void BtRoll_Click(object sender, RoutedEventArgs e)
        {
            if (CoreSingleton.Instance.MegumiControl.GetSession().SelfData.PlayerMatch.MyMD().Gold < 2) return;

            CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_SIMPLE(CommonFeature.Const.E_PACKET.ROLLMONSTERS);
        }
    }
}
