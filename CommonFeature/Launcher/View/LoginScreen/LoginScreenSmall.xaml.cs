﻿using CommonFeature.Const;
using CommonFeature.Translator;
using Launcher.Components.Text;
using Launcher.Model;
using Launcher.Model.LoginScreen;
using Launcher.Properties;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Launcher.View.LoginScreen
{
    /// <summary>
    /// Interaction logic for LoginScreen.xaml
    /// </summary>
    public partial class LoginScreenSmall : Window
    {
        private MediaPlayer BackgroundMusic = new MediaPlayer();
        bool debug = false;

        ModelLogin ML = new ModelLogin();

        private WaveOutEvent outputDevice;
        private AudioFileReader audioFile;

        bool noUpdates = false;

        public LoginScreenSmall(bool debug)
        {
            this.debug = debug;

            InitializeComponent();

            CoreSingleton.Instance.Views.Add(CommonFeature.Const.E_VIEW.E_LOGINSCREEN, this);

            //if (debug)
            //{
            //    CoreSingleton.Instance.StartGame(@"\roZeroClient.exe", "admin", "admin");
            //    this.Close();
            //    return;
            //}

            ML.GetNews(this);

            LoadNewList();

            this.Cursor = new Cursor(new System.IO.MemoryStream(Properties.Resources.RagCur));
            var cursor = new Cursor(new System.IO.MemoryStream(Properties.Resources.RagCurSel));

            foreach (var b in NewBGList)
            {
                b.Cursor = cursor;
            }

            btLogin.Cursor = cursor;
            btPageNew.Cursor = cursor;
            btPageOld.Cursor = cursor;
            btGoMainPage.Cursor = cursor;
            ckRememberPassword.Cursor = cursor;
            ckRememberUsername.Cursor = cursor;
            inpPassword.Cursor = cursor;
            inpUsername.Cursor = cursor;

            LoadNews();

            LoadVideo(Settings.Default.CONF_VideoAutoPlay);
            LoadMusic(Settings.Default.CONF_AutoPlay);
        }

        List<Border> NewBGList = new List<Border>();
        List<BeautyTextBlock> NewTitleList = new List<BeautyTextBlock>();
        List<BeautyTextBlock> NewDescList = new List<BeautyTextBlock>();

        private void LoadNewList()
        {
            NewBGList.Add(news_main);
            NewBGList.Add(news_skin);
            NewBGList.Add(news_event);
            NewBGList.Add(news_card);
            NewBGList.Add(news_offtopic);
            NewBGList.Add(news_community);

            NewTitleList.Add(news_main_title);
            NewTitleList.Add(news_skin_title);
            NewTitleList.Add(news_event_title);
            NewTitleList.Add(news_card_title);
            NewTitleList.Add(news_offtopic_title);
            NewTitleList.Add(news_community_title);

            NewDescList.Add(news_main_notice);
            NewDescList.Add(news_skin_notice);
            NewDescList.Add(news_event_notice);
            NewDescList.Add(news_card_notice);
            NewDescList.Add(news_offtopic_notice);
            NewDescList.Add(news_community_notice);
        }

        public void SetPB(double val, string whoIsLoading)
        {
            Application.Current.Dispatcher.InvokeAsync(new Action(() =>
            {
                pbUpdate.Value = val;
                lbUpdProgress.Content = whoIsLoading;
                //lbLoading.Content = whoIsLoading;
                //Console.WriteLine(val);

                if (whoIsLoading.Equals("Finished"))
                {
                    UCLoading.Visibility = Visibility.Hidden;
                    txLogin.Text = TranslatorSingleton.Instance.GetText("LOGINSCREEN", "LOGIN");
                    lbUpdProgress.Visibility = Visibility.Hidden;

                    var uri = new Uri("pack://application:,,,/Resources/LoginScreen/LoginEnabled.png");

                    ImageBrush ib = new ImageBrush();
                    ib.ImageSource = new BitmapImage(uri);

                    btLogin.Background = ib;

                    System.Media.SoundPlayer player = new System.Media.SoundPlayer(CoreSingleton.Instance.ResourcesPath[E_RESOURCES.SFX] + @"/ACTION/ACTION_COMPLETE.wav");
                    player.Play();

                    noUpdates = true;
                }
            }));
        }

        public void DisplayNew(string folder, List<NewsData> ND)
        {
            for (int i = 0; i < 6; i++)
            {
                var image = new Image();
                var fullFilePath = E_WEB.NEWS_URL_BASE + folder + @"/" + ND[i].IMG;

                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(fullFilePath, UriKind.Absolute);
                bitmap.EndInit();

                NewBGList[i].Background = new ImageBrush(bitmap);
                NewTitleList[i].Text = ND[i].NEWS[0].title;
                NewDescList[i].Text = ND[i].NEWS[0].desc;
            }
        }

        private void LoadNews()
        {

        }

        public void HideScreen()
        {
            if (!debug)
            {
                BackGroundVideo.SourceProvider.MediaPlayer.Pause();
                BackgroundMusic.Stop();
            }
            Hide();
        }

        private void LoadMusic(bool autoplay)
        {
            BackgroundMusic.Open(new Uri(Directory.GetCurrentDirectory() + @"\Resources\Data\BG.mp3"));
            BackgroundMusic.Volume = 0.15;
            BackgroundMusic.MediaEnded += new EventHandler(BGM_Loop);

            //if(autoplay)
            BackgroundMusic.Play();
        }

        private void BGM_Loop(object sender, EventArgs e)
        {
            BackgroundMusic.Position = TimeSpan.Zero;
            BackgroundMusic.Play();
        }

        private void LoadVideo(bool autoplay)
        {
            var vlcLibDirectory = new DirectoryInfo(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "libvlc", IntPtr.Size == 4 ? "win-x86" : "win-x64"));

            var options = new string[]
           {
                "--input-repeat=9999"
               //"-vvv", "--extraintf=logger", "--logfile=Logs.log"
           };

            BackGroundVideo.SourceProvider.CreatePlayer(vlcLibDirectory, options);
            BackGroundVideo.SourceProvider.MediaPlayer.Audio.Volume = 0;
            BackGroundVideo.SourceProvider.MediaPlayer.Rate = 2.00f;

            BackGroundVideo.SourceProvider.MediaPlayer.Play(new Uri(Directory.GetCurrentDirectory() + @"\Resources\Data\BG.mp4"), "screen-fps=60.000000");
            BackGroundVideo.Background.Opacity = 0;

            if (autoplay)
            {
                //BackGroundVideo.SourceProvider.MediaPlayer.Play(new Uri(Directory.GetCurrentDirectory() + @"\Resources\Data\BG.mp4"), "screen-fps=60.000000");
                //BackGroundVideo.Background.Opacity = 0;
            }
        }

        private void BtClose_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            CoreSingleton.Instance.Finish();
        }

        private void MusicToggle_Checked(object sender, RoutedEventArgs e)
        {
            if (BackgroundMusic == null)
                return;

            //if (musicToggle.IsChecked.Value)
            //    BackgroundMusic.Play();
            //else
            //    BackgroundMusic.Stop();
        }

        private void VideoToggle_Checked(object sender, RoutedEventArgs e)
        {
            //if (BackGroundVideo.SourceProvider.MediaPlayer == null)
            //    return;
            //
            //if (!videoToggle.IsChecked.Value)
            //{
            //    BackGroundVideo.SourceProvider.MediaPlayer.Pause();
            //    BackGroundVideo.Background.Opacity = 1;
            //}
            //else
            //{
            //    BackGroundVideo.SourceProvider.MediaPlayer.Play(new Uri(Directory.GetCurrentDirectory() + @"\Resources\Data\BG.mp4"), "screen-fps=60.000000");
            //    BackGroundVideo.Background.Opacity = 0;
            //}
        }

        private void BtLogin_Click(object sender, RoutedEventArgs e)
        {
            //if (username.Text.Length < 4)
            //{
            //    notification.Message.Content = TranslatorSingleton.Instance.GetText(27);
            //    notification.IsActive = true;
            //    return;
            //}
            //else if(password.Password.Length < 4)
            //{
            //    notification.Message.Content = TranslatorSingleton.Instance.GetText(28);
            //    notification.IsActive = true;
            //    return;
            //}
            //else if(username.Text.Length > 24)
            //{
            //    notification.Message.Content = TranslatorSingleton.Instance.GetText(29);
            //    notification.IsActive = true;
            //    return;
            //}
            //else if(password.Password.Length > 33)
            //{
            //    notification.Message.Content = TranslatorSingleton.Instance.GetText(30);
            //    notification.IsActive = true;
            //    return;
            //}
            //
            //this.Hide();
            //
            //if (BackGroundVideo.SourceProvider.MediaPlayer != null)
            //    BackGroundVideo.SourceProvider.MediaPlayer.Pause();
            //
            //if (BackgroundMusic != null)
            //    BackgroundMusic.Stop();
            //
            //Settings.Default.Save();
            //CoreSingleton.Instance.StartGame(@"\roZeroClient.exe", username.Text, password.Password);


            Settings.Default.Save();
            PageGrid.SelectedIndex = 0;
        }

        private void SnackbarMessage_ActionClick(object sender, RoutedEventArgs e)
        {
            notification.IsActive = false;
        }

        bool buttonClickSoundFinished = true;

        private void News_MouseEnter(object sender, MouseEventArgs e)
        {
            Border b = sender as Border;

            if (outputDevice == null)
            {
                outputDevice = new WaveOutEvent();
                outputDevice.PlaybackStopped += OnPlaybackStopped;
            }

            if (audioFile == null)
            {
                audioFile = new AudioFileReader(CoreSingleton.Instance.ResourcesPath[E_RESOURCES.SFX] + @"/HUD/MOUSE_OVER1.wav");
                audioFile.Volume = 0.05f;
                outputDevice.Init(audioFile);
            }

            outputDevice.Play();
            buttonClickSoundFinished = false;

            var animation = new DoubleAnimation
            {
                From = 0.80,
                To = 1,
                Duration = TimeSpan.FromMilliseconds(100),
                AutoReverse = false
            };

            b.BeginAnimation(OpacityProperty, animation);
        }

        private void OnPlaybackStopped(object sender, StoppedEventArgs e)
        {
            buttonClickSoundFinished = true;
            audioFile.Position = 0;
        }

        private void News_MouseLeave(object sender, MouseEventArgs e)
        {
            Border b = sender as Border;

            outputDevice.Stop();

            var animation = new DoubleAnimation
            {
                From = 1,
                To = 0.80,
                Duration = TimeSpan.FromMilliseconds(200),
                AutoReverse = false
            };

            b.BeginAnimation(OpacityProperty, animation);
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            UCLoading.Visibility = Visibility.Visible;

            new Task(async () =>
            {
                Task.Delay(5000);
                bool b = await CoreSingleton.Instance.Patch.DoUpdateLogic(this);
            }).Start();
        }

        private void CkMarked(object sender, MouseButtonEventArgs e)
        {
            Image im = sender as Image;

            Uri uri = null;

            if (im.Tag.ToString() == "False")
            {
                uri = new Uri("pack://application:,,,/Resources/Commons/HUD/CHECKBOX_MARKED.png");
                im.Tag = "True";
            }
            else
            {
                uri = new Uri("pack://application:,,,/Resources/Commons/HUD/CHECKBOX.png");
                im.Tag = "False";
            }

            im.Source = new BitmapImage(uri);
        }

        private void BtGoMainPage_Click(object sender, RoutedEventArgs e)
        {
            PageGrid.SelectedIndex = 1;
        }
    }
}
