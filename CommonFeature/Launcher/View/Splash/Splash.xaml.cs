﻿using CommonFeature.Const;
using CommonFeature.Translator;
using Launcher.Model;
using Launcher.Model.Splash;
using Launcher.Model.Text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Launcher.View.Splash
{
    /// <summary>
    /// Interaction logic for Splash.xaml
    /// </summary>
    public partial class Splash : Window
    {
        bool debug = true;

        public Splash()
        {
            CoreSingleton.Instance.Views.Add(E_VIEW.E_SPLASHSCREEN, this);
            CoreSingleton.Instance.Init(E_INIT.Basic, new int[] { 0 }, MS.TP);
            CoreSingleton.Instance.Init(E_INIT.Language, new int[] { (int)E_Language.English }, MS.TP);

            InitializeComponent();
        }

        private delegate void Updater(string text);
        private delegate void UpdaterPB(double value);

        public ModelSplash MS = new ModelSplash();

        private void DoLoginScreen()
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                //LoginScreen.LoginScreen lsDebug = new LoginScreen.LoginScreen(debug);
                LoginScreen.LoginScreenSmall lsDebug = new LoginScreen.LoginScreenSmall(debug);
                lsDebug.Show();
                this.Close();
            }));
        }

        public void SetPB(double val, string whoIsLoading)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                //lbLoading.Content = whoIsLoading;

            }));
        }

        private void DoLoad()
        {
            Updater updText = new Updater(UpdateLoading);
            UpdaterPB updPB = new UpdaterPB(UpdatePB);

            E_INIT[] inits = { E_INIT.Basic, E_INIT.Discord, E_INIT.Language, E_INIT.Megumi, E_INIT.Monsters, E_INIT.Patcher };

            Dictionary<E_INIT, int[]> parameters = new Dictionary<E_INIT, int[]>();

            parameters.Add(E_INIT.Basic, new int[] { 0 }); // Basic
            parameters.Add(E_INIT.Discord, new int[] { 0 }); // Discord
            parameters.Add(E_INIT.Language, new int[] { (int)E_Language.English }); // Language
            parameters.Add(E_INIT.Megumi, new int[] { 0 }); // Megumi
            parameters.Add(E_INIT.Monsters, new int[] { 0 }); // Monster
            parameters.Add(E_INIT.Patcher, new int[] { 0 }); // Patcher

            foreach (E_INIT e in inits)
            {
                CoreSingleton.Instance.Init(e, parameters[e], MS.TP);

                //Thread.Sleep(1000);
            }

            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                this.Hide();
            }));

            DoLoginScreen();
        }

        private void UpdateLoading(string text)
        {
            //lbLoading.Content = text;
        }

        private void UpdatePB(double value)
        {
            //pbLoad.Value = value;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            Task.Run(() => {
                DoLoad();
            });
        }
    }
}
