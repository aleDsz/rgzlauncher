﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonFeature.Objects
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string UP { get; set; }

        public ulong Account { get; set; }
        public ulong LoginID1 { get; set; }
        public ulong LoginID2 { get; set; }
        public ulong CharID { get; set; }

        public Master SelfData { get; set; }
    }
}
