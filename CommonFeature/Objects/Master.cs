﻿using CommonFeature.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonFeature.Match;

namespace CommonFeature.Objects
{
    public class Master
    {
        public int Account_id = 0;
        public string Name = "????";

        public MasterStats Stats = new MasterStats();
        public MasterMoney Money = new MasterMoney();
        public MasterAvatar Avatar = new MasterAvatar();

        public Match.Match PlayerMatch;

        public class MasterStats
        {
            public int level = 1;
            public int exp = 0;
        }

        public class MasterMoney
        {
            public int TP = 0;
            public int RP = 0;
        }
    }

    public class MasterAvatar
    {
        public E_SEX Sex;

        public AvatarBody Body = new AvatarBody();
        public AvatarHair Hair = new AvatarHair();

        public class AvatarBody
        {
            public int Num;
            public int Color;
        }

        public class AvatarHair
        {
            public int Num;
            public int Color;
        }
    }
}
