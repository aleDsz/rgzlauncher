﻿using CommonFeature.Const;
using CommonFeature.Translator;
using ExtendedRichTextBoxLibNuGet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CommonFeature.Objects
{
    public class Monster : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int _id;
        private int _level;
        private string _name;
        private string _ultimate_desc;
        private string _history;
        private string _voiceactor;
        private short cost;
        private int _life;
        private int _life_max;
        private int _mana_start;
        private int _mana_max;
        private int _def_magic;
        private int _def_atk;
        private int _atk_min;
        private int _atk_max;
        private int _atk_delay;
        private int _atk_range;
        private int _atk_critical;
        private int _move_speed;

        public ExtendedRichTextBox RTBDescricao;

        public List<E_RACES> RACES { get; set; }
        public List<E_CLASS> CLASS { get; set; }
        public List<E_ELEMENTS> ELEMENTS { get; set; }

        public int Id { get => _id; set { _id = value; RaisePropertyChanged(); } }
        public int Level { get => _level; set { _level = value; RaisePropertyChanged(); } }
        public string Name { get => _name; set { _name = value; RaisePropertyChanged(); } }
        public string Ultimate_desc { get => _ultimate_desc; set { _ultimate_desc = value; RaisePropertyChanged(); RTBDescricao = ParseRMTText(_ultimate_desc); } }
        public string History { get => _history; set { _history = value; RaisePropertyChanged(); } }
        public string Voiceactor { get => _voiceactor; set { _voiceactor = value; RaisePropertyChanged(); } }
        public short Cost { get => cost; set { cost = value; RaisePropertyChanged(); } }
        public int Life { get => _life; set { _life = value; RaisePropertyChanged(); } }
        public int Life_max { get => _life_max; set { _life_max = value; RaisePropertyChanged(); } }
        public int Mana_Start { get => _mana_start; set { _mana_start = value; RaisePropertyChanged(); } }
        public int Mana_max { get => _mana_max; set { _mana_max = value; RaisePropertyChanged(); } }
        public string Mana { get => _mana_start + "/" + _mana_max; set { RaisePropertyChanged(); } }
        public int Def_magic { get => _def_magic; set { _def_magic = value; RaisePropertyChanged(); } }
        public int Def_atk { get => _def_atk; set { _def_atk = value; RaisePropertyChanged(); } }
        public int Atk_min { get => _atk_min; set { _atk_min = value; RaisePropertyChanged(); } }
        public int Atk_max { get => _atk_max; set { _atk_max = value; RaisePropertyChanged(); } }
        public string Atk { get => _atk_min + " ~ " + _atk_min; set { RaisePropertyChanged(); } }
        public int Atk_delay { get => _atk_delay; set { _atk_delay = value; RaisePropertyChanged(); } }
        public int Atk_range { get => _atk_range; set { _atk_range = value; RaisePropertyChanged(); } }
        public int Atk_critical { get => _atk_critical; set { _atk_critical = value; RaisePropertyChanged(); } }
        public int Move_speed { get => _move_speed; set { _move_speed = value; RaisePropertyChanged(); } }

        private void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ExtendedRichTextBox CloneRTBDesc()
        {
            ExtendedRichTextBox ERT = new ExtendedRichTextBox();

            if (RTBDescricao == null) return ERT;

            FlowDocument from = RTBDescricao.Document;
            FlowDocument to = ERT.Document;

            TextRange range = new TextRange(from.ContentStart, from.ContentEnd);
            MemoryStream stream = new MemoryStream();
            System.Windows.Markup.XamlWriter.Save(range, stream);
            range.Save(stream, DataFormats.XamlPackage);
            TextRange range2 = new TextRange(to.ContentEnd, to.ContentEnd);
            range2.Load(stream, DataFormats.XamlPackage);

            ERT.Height = Double.NaN;
            ERT.Width = 300;

            ERT.Background = Brushes.Black;
            ERT.Opacity = 0.80;

            return ERT;
        }

        private ExtendedRichTextBox ParseRMTText(string text)
        {
            ExtendedRichTextBox t = null;

            //return t;

            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                t = new ExtendedRichTextBox();

                t.Background = Brushes.Black;
                t.Opacity = 0.80;

                char[] caracteres = text.ToCharArray();

                bool isGettingData = false;
                bool isGettinDataType = false;

                bool isData = false;

                string data_type = "";
                string data_completed = "";

                Paragraph p = new Paragraph();

                Brush bDefault = Brushes.White;
                Brush b = Brushes.White;

                foreach (char c in caracteres)
                {
                    string s = (c.ToString(CultureInfo.InvariantCulture));

                    if (s.Equals("|"))
                    {
                        t.AppendText("\n", bDefault);
                        continue;
                    }

                    p = new Paragraph();
                    p.FontSize = 10;

                    if (s.Equals("@") && !isGettingData)
                    {
                        isGettingData = true;
                        continue;
                    }
                    else if (isGettingData && !isData)
                    {
                        if (s.Equals("(")) // DATA( 
                        {
                            t.AppendText("[");
                            isGettinDataType = true;
                            continue;
                        }

                        if (!isGettinDataType)
                            continue;

                        if (isGettinDataType)
                        {
                            if (s.Equals(","))
                            {
                                switch (data_type)
                                {
                                    case "INFO":
                                    case "ANGEL":
                                        b = Brushes.LightYellow;
                                        break;

                                    case "MAGIC":
                                    case "WATER":
                                    case "MANA":
                                        b = Brushes.LightBlue;
                                        break;

                                    case "STUN":
                                        b = Brushes.Gold;
                                        break;

                                    case "ANIMAL":
                                    case "WIND":
                                    case "TIME":
                                        b = Brushes.ForestGreen;
                                        break;

                                    case "SILENCE":
                                        b = Brushes.LightSteelBlue;
                                        break;

                                    case "CONFUSE":
                                        b = Brushes.LavenderBlush;
                                        break;

                                    case "CHANCE":
                                        b = Brushes.Pink;
                                        break;

                                    case "PLANT":
                                    case "HEAL":
                                        b = Brushes.LightGreen;
                                        break;

                                    case "FREEZE":
                                        b = Brushes.Cyan;
                                        break;

                                    case "CONTACT":
                                    case "ATTACK":
                                    case "BLOOD":
                                    case "FIRE":
                                    case "DAMAGE":
                                    case "DEMIHUMAN":
                                    case "DEMON":
                                    case "REMOVE":
                                    case "BURN":
                                        b = Brushes.Red;
                                        break;

                                    case "DRAGON":
                                        b = Brushes.Orange;
                                        break;

                                    case "CHARM":
                                        b = Brushes.Pink;
                                        break;

                                    case "NEUTRAL":
                                    case "HUMAN":
                                        b = Brushes.LightGray;
                                        break;

                                    case "INSECT":
                                    case "EARTH":
                                    case "SNARE":
                                    case "DISARM":
                                        b = Brushes.RosyBrown;
                                        break;

                                    case "GHOST":
                                        b = Brushes.GhostWhite;
                                        break;

                                    case "UNDEAD":
                                    case "FORMLESS":
                                        b = Brushes.DarkSeaGreen;
                                        break;

                                    case "HOLY":
                                    case "GOD":
                                    case "PARALYZE":
                                        b = Brushes.LightGoldenrodYellow;
                                        break;

                                    case "SHADOW":
                                        b = Brushes.Khaki;
                                        break;

                                    case "CURSED":
                                    case "ROBOT":
                                        b = Brushes.MediumPurple;
                                        break;

                                    case "MACKUP":
                                        b = Brushes.LightSlateGray;
                                        break;
                                }

                                isData = true;
                                continue;
                            }

                            if (s.Equals(")"))
                            {
                                switch (data_type)
                                {
                                    case "ELEMENT_UNDEAD":
                                        b = Brushes.DarkSeaGreen;
                                        data_completed = TranslatorSingleton.Instance.GetText("ELEMENTS", "UNDEAD");
                                        break;
                                }

                                isData = true;
                                continue;
                            }

                            if (!data_type.Equals(" "))
                                data_type += s;
                            continue;
                        }
                    }

                    if (!isData)
                        t.AppendText(s.ToString(), bDefault);
                    else
                    {
                        if (s.Equals(")") || !string.IsNullOrEmpty(data_completed)) // Fim da Data
                        {
                            if (s.Equals(")"))
                            {
                                t.AppendText(" ]", bDefault);

                                if (data_type == "CHANCE")
                                    t.AppendText("%", bDefault);
                            }
                            else
                            {
                                t.AppendText(" " + data_completed, b);
                                t.AppendText(" ]", bDefault);

                                data_completed = "";
                            }

                            isGettingData = false;
                            isGettinDataType = false;
                            isData = false;
                            data_type = "";

                        }
                        else
                        {
                            t.AppendText(s.ToString().Replace(@"/", @" / "), s.Equals(@"/") ? bDefault : b);
                        }
                    }
                }

                t.Height = Double.NaN;
                t.Width = 300;

            });

            return t;
        }
    }
}
