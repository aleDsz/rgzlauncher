﻿using CommonFeature.Const;
using CommonFeature.Objects;
using Megumi.Packet;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Megumi
{
    public class Megumi
    {
        private User session;
        private Packet.PacketParser PkParser;
        private Packet.MEGPK PKSend;
        private Connection.Connection con;

        private ConcurrentQueue<string> packetsStack = new ConcurrentQueue<string>();
        
        public Megumi(string username, string password, bool debug)
        {
            session = new User() { Username = username, Password = password };

            session.UP = username + password;

            PKSend = new Packet.MEGPK(this);

            PkParser = new Packet.PacketParser(this);

            con = new Connection.Connection(this, username.Equals("debug"));
        }

        public MEGPK GetPacketSender()
        {
            return PKSend;
        }

        public Connection.Connection GetConnection()
        {
            return con;
        }

        public User GetSession()
        {
            return session;
        }

        public PacketParser GetPacketParser()
        {
            return PkParser;
        }

        public ConcurrentQueue<string> GetPacketQueue()
        {
            return packetsStack;
        }
    }
}
