﻿using CommonFeature.Client;
using CommonFeature.Const;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Megumi.Packet
{
    public static class PkUtils
    {
        public static byte[] StringToByteArray(string str, int length)
        {
            return Encoding.GetEncoding(1252).GetBytes(str.PadRight(length, '\0'));
        }
    }

    public class MEGPK
    {
        private ushort Id = (ushort)0x65;
        private Megumi meg;

        // Packets Genericos / Teste
        public MEGPK(Megumi meg)
        {
            this.meg = meg;
        }

        //Packets Simples que não enviam data
        public void PK_SIMPLE(E_PACKET packet)
        {
            Pack(packet, "");
        }

        //Informa que jogador deu mouse down
        public void PK_MOUSEDOW(UInt16 x, UInt16 y)
        {
            if (x == 0 || y == 0)
                return;

            string p = string.Format("{0}:{1}", x, y);

            Pack(E_PACKET.MOUSEDOWN, p);
        }

        //Informa da Compra de 1 Personagem
        public void PK_BUYMONSTER(int monster_id)
        {
            Pack(E_PACKET.BUYMONSTER, monster_id.ToString());
        }

        //RagnaGhostz
        public void PK_TALKING(string msg)
        {
            if (msg.Length >= 200) return;
            Pack(E_PACKET.TALKING, msg);
        }

        public void PK_WHISPER(string target, string msg)
        {
            if (msg.Length >= 200 || msg.Contains("|") || target.Contains("|")) return;
            Pack(E_PACKET.WHISPER, target + "|" + msg);
        }

        public void PK_STICKER(string sticker_id)
        {
            Pack(E_PACKET.STICKER, sticker_id);
        }

        public void PK_STYLE(int[] style)
        {
            Pack(E_PACKET.STYLE, style[0] + ":" + style[1] + ":" + style[2]);
        }

        public void PK_BUYMASTERY(int mastery_id, int mastery_level)
        {
            Pack(E_PACKET.BUY_MASTERY, mastery_id + ":" + mastery_level);
        }

        public void PK_MAC(string mac)
        {
            Pack(E_PACKET.SEND_MAC, mac);
        }

        // Efetua o Login
        public void PK_LOGIN()
        {
            Pack(E_PACKET.MEGUMI_LOGIN, "");
        }

        public void Pack(E_PACKET packet_id, string data)
        {
            byte[] result;

            using (MemoryStream MS = new MemoryStream())
            using (BinaryWriter BW = new BinaryWriter(MS))
            {
                BW.Write(Id);
                BW.Write(PkUtils.StringToByteArray(data, 300));
                BW.Write(PkUtils.StringToByteArray(meg.GetSession().UP, 50));
                BW.Write((uint)packet_id);

                result = MS.ToArray();
            }

            Send(result);
        }

        private void Send(byte[] packet)
        {
            try
            {
                meg.GetConnection().mapserv.Send(packet);
            }
            catch
            {

            }
        }

        public void PK_WALK(ushort x, ushort y)
        {
            byte[] result;

            byte[] xy = new byte[3];

            using (MemoryStream MS = new MemoryStream())
            using (BinaryWriter BW = new BinaryWriter(MS))
            {
                BW.Write((ushort)863);
                BW.Write((ushort)x);
                BW.Write((ushort)y);

                result = MS.ToArray();
            }

            Send(result);
        }
    }
}

