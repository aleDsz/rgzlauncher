﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Megumi.Packet
{
    public class PacketCheck
    {
        public int Size { get; set; }
        public char Splitter { get; set; }
    }

    public class PacketParser
    {
        public Megumi megumi;
        Dictionary<string, PacketCheck> PK = new Dictionary<string, PacketCheck>();

        public PacketParser(Megumi megumi)
        {
            this.megumi = megumi;

            // Connection
            PK.Add("LOADOK", new PacketCheck() { Splitter = '|', Size = 0 });

            // Match
            PK.Add("DRAWMONSTER", new PacketCheck() { Splitter = '|', Size = 2 });

            // Data
            PK.Add("SELFDATA", new PacketCheck() { Splitter = '|', Size = 11 });
            PK.Add("UPDINFO", new PacketCheck() { Splitter = '|', Size = 3 });
        }

        public void CheckPacketIntegrity(string[] packets, StringBuilder sb)
        {
            bool allOk = true;

            for (int i = 0; i < packets.Length; i++)
            {
                if (packets[i] == "")
                    continue;

                string[] cmdParser = packets[i].Split(':');

                if (cmdParser.Length == 1)
                    continue;

                string cmd = cmdParser[1];

                PacketCheck ck = null;

                if(!PK.TryGetValue(cmd, out ck)) //Se o packet não existe, é por que veio incompleto
                {
                    sb.Append(packets[i]);
                    allOk = false;
                    continue;
                }

                if (ck.Size == 0)
                    continue;

                string[] check = cmdParser[2].Split(ck.Splitter);

                if (check.Length < ck.Size)
                {   // Último Packet provavelmente veio Incompleto, vamos aguardar
                    sb.Append(packets[i]);
                    allOk = false;
                }
            }

            if (allOk)
                sb.Clear();
        }

        public bool AddToStack(string packet)
        {
            if (!packet.Contains("CMD:"))
                return false;


            megumi.GetPacketQueue().Enqueue(packet);

            return true;
        }
    }
}
