﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CommonFeature.Const;
using CommonFeature.Objects;
using Megumi.Packet;

namespace Megumi.Connection
{
    public class Connection
    {
        public Megumi megumi;

        uint client_tick = 1000;
        byte[] bytes = new byte[2048];

        public Socket mapserv;

        public bool debug = false;

        public Connection(Megumi megumi, bool debug)
        {
            this.megumi = megumi;
            this.debug = debug;

            new Task(() =>
            {
                Connect(megumi.GetSession().Username, megumi.GetSession().Password);
            }).Start();
        }

        private void Connect(string username, string password)
        {
            mapserv = createSocket(debug ? Const_Connection.ip_localhost : Const_Connection.ip_vps, Const_Connection.mapPort, true);
            mapserv.BeginReceive(new byte[] { 0 }, 0, 0, 0, ReceiveCallbackMapServ, null);

            map_login();
        }

        private Socket createSocket(string ipAdress, int port, bool sendPing)
        {
            Socket soc = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress ip = IPAddress.Parse(ipAdress);
            IPEndPoint host = new IPEndPoint(ip, port);

            soc.Connect(host);

            //if (sendPing)
            //    soc.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.KeepAlive, sendTick());

            return soc;
        }

        StringBuilder sb = new StringBuilder();

        private void ReceiveCallbackMapServ(IAsyncResult result)
        {
            if (mapserv.Available > 0)
            {
                byte[] buffer = new byte[1024];
                mapserv.Receive(buffer);

                string packet = Encoding.Default.GetString(buffer).Replace("�", "");
                packet = packet.Replace("\u0002", "");
                packet = packet.Replace("\u001e", "");
                packet = packet.Replace("\0", "");

                if (!packet.Substring(packet.Length - 2).Equals("<>"))
                {
                    sb.Append(packet);
                    mapserv.BeginReceive(new byte[] { 0 }, 0, 0, 0, ReceiveCallbackMapServ, null);
                    return;
                }

                sb.Clear();

                Console.WriteLine("-----------------------");
                Console.WriteLine(packet);
                Console.WriteLine("-----------------------");

                if (debug)
                {
                    MemoryStream MS = new MemoryStream(buffer);
                    BinaryReader BR = new BinaryReader(MS);
                    Console.WriteLine("Packet ID: " + (int)BR.ReadUInt16());
                }

                string[] packet_split = packet.Split(new string[] { "<>" }, StringSplitOptions.None);

                if (packet_split.Length > 0 && !String.IsNullOrEmpty(packet))
                {
                    foreach (string pk in packet_split)
                        megumi.GetPacketParser().AddToStack(pk);
                }

                mapserv.BeginReceive(new byte[] { 0 }, 0, 0, 0, ReceiveCallbackMapServ, null);
            }
        }

        private void map_login()
        {
            megumi.GetPacketSender().PK_LOGIN();
            Console.WriteLine("(BOT) Conectado ao Map-Serv!");
        }
    }
}
