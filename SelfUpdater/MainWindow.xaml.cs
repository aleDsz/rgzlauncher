﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SelfUpdater
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Focus();

            bool debug = false;

            string launcherExe = @"\RGZLauncher.exe";
            string launchernew = @"\RGZLauncher_New.exe";

            string megumidll = @"\Megumi.dll";
            string meguminew = @"\Megumi_New.dll";

            string commondll = @"\CommonFeature.dll";
            string commonnew = @"\CommonFeature_New.dll";

            string keyboarddll = @"\Open.WinKeyboardHook.dll";
            string keyboardnew = @"\Open.WinKeyboardHook_New.dll";

            string applicationName = "RGZLauncher";

            new Thread(() =>
            {
                Thread.Sleep(3000);

               
                //MessageBox.Show("Clique em OK para prosseguir com a atualização");

                if (debug)
                    MessageBox.Show("Iremos reiniciar");

                if (File.Exists(Directory.GetCurrentDirectory() + launchernew))
                {
                    if (debug)
                        MessageBox.Show("Achamos no Diretório");

                    File.Copy(Directory.GetCurrentDirectory() + launchernew, Directory.GetCurrentDirectory() + launcherExe, true);
                }

                if (File.Exists(Directory.GetCurrentDirectory() + meguminew))
                {
                    if (debug)
                        MessageBox.Show("Achamos no Diretório");

                    File.Copy(Directory.GetCurrentDirectory() + meguminew, Directory.GetCurrentDirectory() + megumidll, true);
                }

                if (File.Exists(Directory.GetCurrentDirectory() + commonnew))
                {
                    if (debug)
                        MessageBox.Show("Achamos no Diretório");

                    File.Copy(Directory.GetCurrentDirectory() + commonnew, Directory.GetCurrentDirectory() + commondll, true);
                }

                if (File.Exists(Directory.GetCurrentDirectory() + keyboardnew))
                {
                    if (debug)
                        MessageBox.Show("Achamos no Diretório");

                    File.Copy(Directory.GetCurrentDirectory() + keyboardnew, Directory.GetCurrentDirectory() + keyboarddll, true);
                }

                Thread.Sleep(1000);

                var p = new Process();

                p.StartInfo.FileName = Directory.GetCurrentDirectory() + launcherExe;

                if (System.Environment.OSVersion.Version.Major >= 6)
                {
                    p.StartInfo.Verb = "runas";
                }

                File.Delete(Directory.GetCurrentDirectory() + commonnew);
                File.Delete(Directory.GetCurrentDirectory() + meguminew);
                File.Delete(Directory.GetCurrentDirectory() + launchernew);
                File.Delete(Directory.GetCurrentDirectory() + keyboardnew);

                Thread.Sleep(3000);

                p.Start();  

                Environment.Exit(0);
            }).Start();
        }

        public static bool IsProcessOpen(string name)
        {
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (clsProcess.ProcessName.Contains(name))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
