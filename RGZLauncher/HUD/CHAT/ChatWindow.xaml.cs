﻿using CommonFeature.Client;
using CommonFeature.Const;
using CommonFeature.Utils;
using MaterialDesignThemes.Wpf;
using Open.WinKeyboardHook;
using RGZLauncher.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace RGZLauncher.HUD.CHAT
{
    /// <summary>
    /// Interaction logic for ChatWindow.xaml
    /// </summary>
    public partial class ChatWindow : System.Windows.Controls.UserControl
    {
        private readonly IKeyboardInterceptor _interceptor;

        public ChatWindow()
        {
            InitializeComponent();

            Cursors.Add(E_CURSOR.NORMAL, new System.Windows.Input.Cursor(new System.IO.MemoryStream(RGZLauncher.Properties.Resources.RagCur)));
            Cursors.Add(E_CURSOR.CLICK, new System.Windows.Input.Cursor(new System.IO.MemoryStream(RGZLauncher.Properties.Resources.RagCurSel)));

            this.Cursor = Cursors[E_CURSOR.NORMAL];
            resizebutton.Cursor = Cursors[E_CURSOR.CLICK];
            sticker.Cursor = Cursors[E_CURSOR.CLICK];

            dialogtab.Height = Settings.Default.CHAT_HCLICK * 20;

            ((System.Windows.Controls.ListBox)stickPopup.PopupContent).Items.Clear();

            _interceptor = new KeyboardInterceptor();
            _interceptor.KeyPress += KeyBoardInterceptor;
            _interceptor.KeyDown += KeyBoardInterceptorDown;
            _interceptor.KeyUp += KeyBoardInterceptorUp;

            _interceptor.StartCapturing();

            SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);
        }

        bool disabled = false;

        public KeyLogic KL = new KeyLogic();

        private bool WASDLogic(char key)
        {
            if (CoreSingleton.Instance.RagHook.GetStringValue(OffSets.MAP.NAME).Contains("login"))
                return false;

            E_WALKDIR newWalkDir = E_WALKDIR.NONE;

            switch (key)
            {
                case 'w':
                case 'W':
                    if (KL.A_IsDown)
                        newWalkDir = E_WALKDIR.DIAGONAL_ESQUERDA_CIMA;
                    else if (KL.D_IsDown)
                        newWalkDir = E_WALKDIR.DIAGONAL_DIREITA_CIMA;
                    else
                        newWalkDir = E_WALKDIR.CIMA;
                    break;

                case 'a':
                case 'A':
                    if (KL.W_IsDown)
                        newWalkDir = E_WALKDIR.DIAGONAL_ESQUERDA_CIMA;
                    else if (KL.S_IsDown)
                        newWalkDir = E_WALKDIR.DIAGONAL_ESQUERDA_BAIXO;
                    else
                        newWalkDir = E_WALKDIR.ESQUERDA;
                    break;

                case 's':
                case 'S':
                    if (KL.A_IsDown)
                        newWalkDir = E_WALKDIR.DIAGONAL_ESQUERDA_BAIXO;
                    else if (KL.D_IsDown)
                        newWalkDir = E_WALKDIR.DIAGONAL_DIREITA_BAIXO;
                    else
                        newWalkDir = E_WALKDIR.BAIXO;
                    break;

                case 'd':
                case 'D':
                    if (KL.W_IsDown)
                        newWalkDir = E_WALKDIR.DIAGONAL_DIREITA_CIMA;
                    else if (KL.S_IsDown)
                        newWalkDir = E_WALKDIR.DIAGONAL_DIREITA_BAIXO;
                    else
                        newWalkDir = E_WALKDIR.DIREITA;
                    break;

                default:
                    return false;
            }

            if (newWalkDir == KL.lastDir)
                return false;

            KL.lastDir = newWalkDir;

            return true;
        }

        private bool altIsUp = false;

        private void KeyBoardInterceptorDown(object sender, System.Windows.Forms.KeyEventArgs key)
        {
            if (nowasd || CoreSingleton.Instance.RagHook.GetStringValue(OffSets.MAP.NAME).Contains("login")) return;

            switch (key.KeyCode)
            {
                case Keys.W:
                    KL.W_IsDown = true;
                    break;

                case Keys.A:
                    KL.A_IsDown = true;
                    break;

                case Keys.S:
                    KL.S_IsDown = true;
                    break;

                case Keys.D:
                    KL.D_IsDown = true;
                    break;

                case Keys.LMenu:
                case Keys.RMenu:
                    altIsUp = true;
                    break;

                case Keys.Down:
                case Keys.Up:
                    if (msgHistory.Count == 0)
                        return;

                    disabled = false;

                    if (key.KeyCode == Keys.Down)
                        history_index--;
                    else
                        history_index++;

                    if (history_index > msgHistory.Count - 1 || history_index < 0)
                        history_index = 0;

                    msg.Text = msgHistory[history_index];
                    return;

                default:
                    return;
            }
        }

        public int totalHold = 0;

        public int history_index;
        public List<string> msgHistory = new List<string>();

        private void KeyBoardInterceptorUp(object sender, System.Windows.Forms.KeyEventArgs key)
        {
            if (nowasd || CoreSingleton.Instance.RagHook.GetStringValue(OffSets.MAP.NAME).Contains("login")) return;

            E_WALKDIR newWalkDir = E_WALKDIR.NONE;

            switch (key.KeyCode)
            {
                case Keys.W:
                    KL.W_IsDown = false;

                    if (KL.A_IsDown)
                        newWalkDir = E_WALKDIR.ESQUERDA;
                    else if (KL.D_IsDown)
                        newWalkDir = E_WALKDIR.DIREITA;
                    break;

                case Keys.A:
                    KL.A_IsDown = false;

                    if (KL.W_IsDown)
                        newWalkDir = E_WALKDIR.CIMA;
                    else if (KL.S_IsDown)
                        newWalkDir = E_WALKDIR.BAIXO;
                    break;

                case Keys.S:
                    KL.S_IsDown = false;

                    if (KL.A_IsDown)
                        newWalkDir = E_WALKDIR.ESQUERDA;
                    else if (KL.D_IsDown)
                        newWalkDir = E_WALKDIR.DIREITA;
                    break;

                case Keys.D:
                    KL.D_IsDown = false;

                    if (KL.W_IsDown)
                        newWalkDir = E_WALKDIR.CIMA;
                    else if (KL.S_IsDown)
                        newWalkDir = E_WALKDIR.BAIXO;
                    break;

                case Keys.LMenu:
                case Keys.RMenu:
                    altIsUp = false;
                    break;

                default:
                    return;
            }

            if (KL.isAllUp())
            {
                CoreSingleton.Instance.player.state.firstWalk = true;
                totalHold = 0;
                KL.lastDir = E_WALKDIR.NONE;
            }
            else if (newWalkDir != E_WALKDIR.NONE)
            {
                KL.lastDir = newWalkDir;

                WASDSend();
            }
        }

        private void WASDSend()
        {
            new Thread(delegate ()
            {
                XY xy = new XY() { x = 0, y = 0 };
                E_WALKDIR threadWalk = KL.lastDir;

                if (KL.lastDir != E_WALKDIR.NONE)
                    xy = xy.walkLogic(KL.lastDir, (int)CoreSingleton.Instance.RagHook.GetFloatValue(OffSets.GetCameraRotationOffset(OffSets.SCREEN.CAMERA_ROTATION, "ROTATION"), false));

                int newX = CoreSingleton.Instance.RagHook.GetIntValue(OffSets.MAP.X, true) + xy.x;
                int newY = CoreSingleton.Instance.RagHook.GetIntValue(OffSets.MAP.Y, true) + xy.y;

                while (KL.lastDir == threadWalk && !KL.isAllUp() && disabled)
                {
                    CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_WALK((ushort)newX, (ushort)newY);

                    xy = xy.walkLogic(threadWalk, (int)CoreSingleton.Instance.RagHook.GetFloatValue(OffSets.GetCameraRotationOffset(OffSets.SCREEN.CAMERA_ROTATION, "ROTATION"), false));

                    newX += xy.x;
                    newY += xy.y;

                    if (threadWalk == E_WALKDIR.CIMA || threadWalk == E_WALKDIR.BAIXO ||
                       threadWalk == E_WALKDIR.ESQUERDA || threadWalk == E_WALKDIR.DIREITA)
                        Thread.Sleep(100);
                    else
                        Thread.Sleep(175);
                }
            }).Start();
        }

        bool nowasd = false;

        private void KeyBoardInterceptor(object sender, KeyPressEventArgs key)
        {
            //Console.WriteLine(key.KeyChar);

            IntPtr hwnd = WinApi.GetForegroundWindow();

            bool thisFocus = WinApi.ApplicationIsActivated();

            string map = CoreSingleton.Instance.RagHook.GetStringValue(OffSets.MAP.NAME);

            if ((hwnd != CoreSingleton.Instance.Client.Game.MainWindowHandle && !thisFocus) || map.Contains("login"))
                return;

            if (!disabled && msg.Text.Length == 0 && (key.KeyChar == '\r' || key.KeyChar == '\n'))
            {
                disabled = true;
                msg.Text = "Pressione [Enter] para ativar a captura de texto do chat";
                return;
            }

            if(altIsUp)
            {
                switch(key.KeyChar.ToString().ToLower())
                {
                    case "q":
                    case "e":
                    case "i":
                    case "a":
                    case "s":
                    case "z":
                    case "g":
                        return;
                }
            }

            if(disabled)
            {
                if (key.KeyChar == '\r' || key.KeyChar == '\n')
                {
                    msg.Clear();
                    disabled = false;
                    return;
                }
                else if(!nowasd && WASDLogic(key.KeyChar))
                {
                    WASDSend();
                }

                return;
            }

            if ((key.KeyChar == '\r' || key.KeyChar == '\n') && msg.Text.Length > 0)
            {
                msg.Text = msg.Text.Trim();

                if (msg.Text.Equals("/nowasd"))
                {
                    nowasd = !nowasd;

                    addText("#BLUE#Andar pelo WASD agora está " + ( nowasd ? "Desativado" : "Ativado" ), "DISP");
                }
                else if(msg.Text.Equals("/clear"))
                {
                    chatbox.Document.Blocks.Clear();
                    grupobox.Document.Blocks.Clear();
                    guildbox.Document.Blocks.Clear();
                }
                else if (!string.IsNullOrEmpty(whisper.Text))
                {
                    if (whisper.Text.Length > 30) return;

                    msgHistory.Add(msg.Text);
                    history_index = msgHistory.Count - 1;

                    CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_WHISPER(whisper.Text, msg.Text);
                }
                else
                {
                    if(!string.IsNullOrEmpty(msg.Text))
                        CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_TALKING(msg.Text);

                    msgHistory.Add(msg.Text);
                    history_index = msgHistory.Count - 1;
                }

                msg.Clear();
            }
            else if(key.KeyChar == '\b' && msg.Text.Length >= 1)
            {
                msg.Text = msg.Text.Remove(msg.Text.Length - 1);
            }
            else if (key.KeyChar != '\r' && key.KeyChar != '\n')
            {
                msg.Text += key.KeyChar;
            }
        }

        Dictionary<E_CURSOR, System.Windows.Input.Cursor> Cursors = new Dictionary<E_CURSOR, System.Windows.Input.Cursor>();


        Point anchorPoint;
        Point currentPoint;
        bool isInDrag = false;
        private TranslateTransform transform = new TranslateTransform();


        private void Dialogbox_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (isInDrag)
            {
                var element = sender as FrameworkElement;
                currentPoint = e.GetPosition(null);

                transform.X += currentPoint.X - anchorPoint.X;
                transform.Y += (currentPoint.Y - anchorPoint.Y);
                this.RenderTransform = transform;
                anchorPoint = currentPoint;
            }
        }

        private void Dialogbox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (isInDrag)
            {
                var element = sender as FrameworkElement;
                element.ReleaseMouseCapture();
                isInDrag = false;
                e.Handled = true;
            }
        }

        private void Dialogbox_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var element = sender as FrameworkElement;
            anchorPoint = e.GetPosition(null);
            element.CaptureMouse();
            isInDrag = true;
            e.Handled = true;
        }

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hwnd);

        private void UserControl_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            CoreSingleton.Instance.RagHook.MoveCursor(9999, 0);
            SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);
        }

        private void UserControl_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SetForegroundWindow(CoreSingleton.Instance.Client.Game.MainWindowHandle);
        }

        Thickness defaultresizemargin = new Thickness(572, 222, 0, 0);

        private void Resizebutton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Settings.Default.CHAT_HCLICK++;

            dialogtab.Height = Settings.Default.CHAT_HCLICK * 20;

            if (dialogtab.Height > 400)
            {
                Settings.Default.CHAT_HCLICK = 1;
                dialogtab.Height = 20;
            }

            Settings.Default.Save();
        }

        private void TextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            
        }

        private void Sticker_MouseDown(object sender, MouseButtonEventArgs e)
        {
            stickPopup.IsPopupOpen = true;
            stickPopup.Height = 20;
        }

        private void lvStick_sendSticker(object sender, MouseButtonEventArgs e)
        {
            //if (!Util.ChatControl.canSendMsg(chatMsgs, true, txMsg.Text, spam_count, pd)) return;

            stickPopup.IsPopupOpen = false;

            var item = sender as ListBoxItem;

            if (item != null)
            {
                CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_STICKER(((Image)item.Content).Tag.ToString());
            }
        }

        public void clearChats()
        {
            chatbox.Document.Blocks.Clear();
            grupobox.Document.Blocks.Clear();
            guildbox.Document.Blocks.Clear();
            serverbox.Document.Blocks.Clear();

            mainToRead.Badge = 0;
            partyToRead.Badge = 0;
            guildToRead.Badge = 0;
            globalToRead.Badge = 0;

            ((System.Windows.Controls.ListBox)stickPopup.PopupContent).Items.Clear();
        }

        string actual_tab = "";

        public void addText(string text, string chat)
        {
            if (text.Length < 1 || string.IsNullOrEmpty(text)) return;

            bool emTodosChats = false;

            System.Windows.Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                Brush b = Brushes.White;

                if (text.Substring(0, 1).Equals("#"))
                {
                    string strValue = Utils.pegaValorEntre(text, "#", "#");
                    string stremove = "#" + strValue + "#";
                    string path = CoreSingleton.Instance.ResourcesPath[E_RESOURCES.ChatIcons];

                    if(strValue.Equals(CoreSingleton.Instance.RagHook.GetStringValue(OffSets.Player.CHAR_NAME)))
                    {
                        b = Brushes.LimeGreen;
                    }
                    else if (strValue.Contains("MAST_"))
                    {
                        string[] data = strValue.Split('_');

                        path = CoreSingleton.Instance.ResourcesPath[E_RESOURCES.MasteryImages];

                        emTodosChats = true;

                        addImage(path + data[1] + ".png");
                    }
                    else
                    {
                        switch (strValue)
                        {
                            case "GREEN":
                                b = Brushes.LightGreen;
                                emTodosChats = true;
                                break;

                            case "ANNOUNCE":
                                b = Brushes.Yellow;
                                emTodosChats = true;
                                break;

                            case "ERRO":
                                b = Brushes.Red;
                                emTodosChats = true;
                                break;

                            case "WHISPER":
                                b = Brushes.LightPink;
                                emTodosChats = true;
                                break;

                            case "BLUE":
                                b = Brushes.LightSkyBlue;
                                emTodosChats = true;
                                break;

                            case "ORANGE":
                                b = Brushes.Orange;
                                emTodosChats = true;
                                break;

                            default:
                                b = Brushes.White;
                                break;
                        }

                        addImage(path + stremove + ".png");
                    }

                    text = text.Replace(stremove, "");
                }

                if (string.IsNullOrEmpty(text)) return;

                if (text.Substring(0, 1).Equals("*"))
                {
                    b = Brushes.LightSkyBlue;
                }

                if (chat.Equals("DISP") || emTodosChats)
                {
                    chatbox.Height = Double.NaN;
                    chatbox.AppendText(text + "\n", b);
                    chatbox.ScrollToEnd();

                    if (!actual_tab.Equals("TAG_Principal") && !emTodosChats)
                        mainToRead.Badge = Int32.Parse(mainToRead.Badge.ToString()) + 1;
                }

                if (chat.Equals("DISPPARTY") || emTodosChats)
                {
                    grupobox.Height = Double.NaN;
                    grupobox.AppendText(text.Replace("/p", "") + "\n", b);
                    grupobox.ScrollToEnd();

                    if (!actual_tab.Equals("TAG_Grupo") && !emTodosChats)
                        partyToRead.Badge = Int32.Parse(partyToRead.Badge.ToString()) + 1;
                }

                if (chat.Equals("DISPGUILD") || emTodosChats)
                {
                    guildbox.Height = Double.NaN;
                    guildbox.AppendText(text.Replace("/g", "") + "\n", b);
                    guildbox.ScrollToEnd();

                    if (!actual_tab.Equals("TAG_Cla") && !emTodosChats)
                        guildToRead.Badge = Int32.Parse(guildToRead.Badge.ToString()) + 1;
                }

                if (chat.Equals("DISPSERVER"))
                {
                    serverbox.Height = Double.NaN;
                    serverbox.AppendText(text.Replace("/a", "") + "\n", b);
                    serverbox.ScrollToEnd();

                    if (!actual_tab.Equals("TAG_Global") && !emTodosChats)
                        globalToRead.Badge = Int32.Parse(globalToRead.Badge.ToString()) + 1;
                }
            }));
        }

        public void enableSticker(string sticker_id)
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (File.Exists(CoreSingleton.Instance.ResourcesPath[E_RESOURCES.StickerImages] + sticker_id + ".png"))
                {
                    var stick = new BitmapImage();
                    stick.BeginInit();
                    stick.UriSource = new Uri(CoreSingleton.Instance.ResourcesPath[E_RESOURCES.StickerImages] + sticker_id + ".png");
                    stick.EndInit();

                    Image png = new Image();
                    png.Source = stick;
                    png.Tag = sticker_id.ToString();

                    ((System.Windows.Controls.ListBox)stickPopup.PopupContent).Items.Add(png);
                }
            }));
        }

        public void addSticker(string sticker_id)
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                addImage(CoreSingleton.Instance.ResourcesPath[E_RESOURCES.StickerImages] + sticker_id + ".png", 150, 120);
            }));
        }

        private void addImage(string image_path, int w = 0, int h = 0)
        {
            if (File.Exists(image_path))
            {
                Paragraph para = new Paragraph();

                BitmapImage bitmap = new BitmapImage(new Uri(image_path));
                System.Windows.Controls.Image image = new System.Windows.Controls.Image();
                image.Source = bitmap;

                if (w > 0)
                    image.Width = w;

                if (h > 0)
                    image.Height = h;

                if( w == 0 || h == 0)
                    image.Stretch = Stretch.None;

                image.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                para.Inlines.Add(image);

                chatbox.Document.Blocks.Add(para);

                chatbox.ScrollToEnd();
            }
        }

        private void BadgeChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var badge = sender as Badged;

            if (badge.Badge.ToString().Equals("0"))
                badge.Visibility = Visibility.Collapsed;
            else
            {
                if (Int32.Parse(badge.Badge.ToString()) > 99)
                    badge.Badge = "99";

                badge.Visibility = Visibility.Visible;
            }
        }

        private void Dialogtab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var tab = (TabItem)dialogtab.SelectedItem;

            actual_tab = tab.Tag.ToString();

            switch (actual_tab)
            {
                case "TAG_Grupo":
                    partyToRead.Badge = "0";
                    break;

                case "TAG_Cla":
                    guildToRead.Badge = "0";
                    break;

                case "TAG_Global":
                    globalToRead.Badge = "0";
                    break;

                case "TAG_Principal":
                    mainToRead.Badge = "0";
                    break;
            }

        }
    }
}
