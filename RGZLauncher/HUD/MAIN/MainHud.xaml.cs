﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CommonFeature.Client;
using CommonFeature.Const;
using RGZLauncher.GameLogic;

namespace RGZLauncher.HUD.MAIN
{
    /// <summary>
    /// Interaction logic for MainHud.xaml
    /// </summary>
    public partial class MainHud : HookWindow
    {
        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hwnd);

        public MainHud()
        {
            InitializeComponent();
            MakeView(E_VIEW.E_MAINHUD);
            SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);

            Refresh(null, null);
        }

        public override void RectLogic(RECT rect)
        {
            MoveWindow(CoreSingleton.Instance.Client.Game.MainWindowHandle, (int)Left, (int)Top, rect.right - rect.left, rect.bottom - rect.top, true);
        }

        private void BtClose_MouseEnter(object sender, MouseEventArgs e)
        {
        }

        private void BtClose_MouseLeave(object sender, MouseEventArgs e)
        {
        }

        private void BtClose_MouseUp(object sender, MouseButtonEventArgs e)
        {
            CoreSingleton.Instance.Finish();
        }

        public override void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //this.DragMove();
            }
            catch (Exception ex)
            {

            }
        }

        public override void Window_LocationChanged(object sender, EventArgs e)
        {
            
        }
    }
}

