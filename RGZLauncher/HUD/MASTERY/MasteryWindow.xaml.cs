﻿using CommonFeature.Client;
using CommonFeature.Const;
using RGZLauncher.GameLogic;
using RGZLauncher.GameLogic.OBJ;
using RGZLauncher.HUD.MAIN;
using RGZLauncher.HUD.MASTERY.UC;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RGZLauncher.HUD.MASTERY
{
    /// <summary>
    /// Interaction logic for MasteryWindow.xaml
    /// </summary>
    public partial class MasteryWindow : UserControl
    {
        public Player player;
        E_MASTERYCURRENCY type;

        public ObservableCollection<MasterySkill> ocMaestria { get; set; } = new ObservableCollection<MasterySkill>();

        public Dictionary<int, Mastery> dbMastery = new Dictionary<int, Mastery>();

        public bool bCanOk = false;

        // SKILL_ID, SKILL_NOVO_LEVEL
        public Dictionary<int, int> myChanges { get; set; } = new Dictionary<int, int>();

        Dictionary<E_CURSOR, Cursor> Cursors = new Dictionary<E_CURSOR, Cursor>();

        public MasteryWindow( )
        {
            InitializeComponent();

            Cursors.Add(E_CURSOR.NORMAL, new Cursor(new System.IO.MemoryStream(RGZLauncher.Properties.Resources.RagCur)));
            Cursors.Add(E_CURSOR.CLICK, new Cursor(new System.IO.MemoryStream(RGZLauncher.Properties.Resources.RagCurSel)));

            this.Cursor = Cursors[E_CURSOR.NORMAL];
            this.btClose.Cursor = Cursors[E_CURSOR.CLICK];
            this.btOk.Cursor = Cursors[E_CURSOR.CLICK];
            this.lbClasse.Cursor = Cursors[E_CURSOR.CLICK];
            this.lbEvento.Cursor = Cursors[E_CURSOR.CLICK];
            this.lbInstancias.Cursor = Cursors[E_CURSOR.CLICK];

            Visibility = Visibility.Collapsed;

            SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);
        }

        public void Load(Player player)
        {
            this.player = player;

            dbMastery = CoreSingleton.Instance.dbMastery.Masteries;

            LoadMasteryData(E_MASTERYCURRENCY.CONNECTION);

            SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);

            Visibility = Visibility.Visible;
        }

        public void updatePoints(E_MASTERYCURRENCY type)
        {
            switch (type)
            {
                case E_MASTERYCURRENCY.CONNECTION:
                    txPoints.Content = "Pontos de Fidelidade: " + player.points[(int)type];
                    tabEvento.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/MASTERY/Texture/MASTERY_TABSEL.png"));
                    break;

                case E_MASTERYCURRENCY.INSTANCE:
                    txPoints.Content = "Não há pontos disponíveis";
                    tabInstancias.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/MASTERY/Texture/MASTERY_TABSEL.png"));
                    break;

                case E_MASTERYCURRENCY.EVENT:
                    txPoints.Content = "Pontos de Evento: " + player.points[(int)type];
                    tabClasse.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/MASTERY/Texture/MASTERY_TABSEL.png"));
                    break;
            }
        }

        private void LoadMasteryData(E_MASTERYCURRENCY type)
        {
            this.type = type;

            myChanges.Clear();

            ocMaestria = new ObservableCollection<MasterySkill>();

            tabClasse.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/MASTERY/Texture/MASTERY_TABUNSEL.png"));
            tabEvento.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/MASTERY/Texture/MASTERY_TABUNSEL.png"));
            tabInstancias.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/MASTERY/Texture/MASTERY_TABUNSEL.png"));

            updatePoints(type);

            foreach (KeyValuePair<int, int> md in player.masteries)
            {
                if (!dbMastery.ContainsKey(md.Key) ||
                    dbMastery[md.Key].type != (int)type) continue;

                MasterySkill MS = new MasterySkill(dbMastery[md.Key], md.Value);
                ocMaestria.Add(MS);
            }

            listaMaestrias.ItemsSource = ocMaestria;
        }

        private void TabClick(object sender, MouseButtonEventArgs e)
        {
            Label lb = sender as Label;

            E_MASTERYCURRENCY seltype = (E_MASTERYCURRENCY)Int32.Parse(lb.Tag.ToString());

            LoadMasteryData(seltype);
        }

        private void btOk_MouseEnter(object sender, MouseEventArgs e)
        {
            //MouseEnterCursor(sender, e);

            if (bCanOk)
                btOk.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/GlobalTexture/BT_OK.png"));
        }

        private void btOk_MouseLeave(object sender, MouseEventArgs e)
        {
            if (bCanOk)
                btOk.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/GlobalTexture/BT_OK.png"));
            else
                btOk.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/GlobalTexture/BT_OK_DISABLED.png"));
        }

        private void btOk_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (myChanges.Count == 0 || !bCanOk) return;

            bCanOk = false;
            btOk.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/GlobalTexture/BT_OK_DISABLED.png"));

            foreach(KeyValuePair<int, int> m in myChanges)
            {
                if (m.Value == player.masteries[m.Key]) continue;

                CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_BUYMASTERY(m.Key, m.Value);

                Thread.Sleep(100);
            }

            myChanges.Clear();

            CoreSingleton.Instance.player.state.mastery = false;
            CoreSingleton.Instance.GameUC.Remove(CommonFeature.Const.E_VIEW.E_MASTERYSCREEN);
            CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_SIMPLE(E_PACKET.CLOSED_MASTERY);
            ((MainHud)CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).maingrid.Children.Remove(this);
            Visibility = Visibility.Collapsed;
        }

        public void MouseEnterCursor(object sender, MouseEventArgs e)
        {
            CoreSingleton.Instance.RagHook.MoveCursor(9999, 0);
            SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);
        }

        private void CloseScreen(object sender, MouseButtonEventArgs e)
        {
            CoreSingleton.Instance.player.state.mastery = false;
            CoreSingleton.Instance.GameUC.Remove(CommonFeature.Const.E_VIEW.E_MASTERYSCREEN);
            CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_SIMPLE(E_PACKET.CLOSED_MASTERY);
            Visibility = Visibility.Collapsed;
        }

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hwnd);
        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
        }

        private void HookWindow_MouseLeave(object sender, MouseEventArgs e)
        {
            SetForegroundWindow(CoreSingleton.Instance.Client.Game.MainWindowHandle);
        }

        Point anchorPoint;
        Point currentPoint;
        bool isInDrag = false;

        private void Lbdrag_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var element = sender as FrameworkElement;
            anchorPoint = e.GetPosition(null);
            element.CaptureMouse();
            isInDrag = true;
            e.Handled = true;
        }

        private TranslateTransform transform = new TranslateTransform();

        private void Lbdrag_MouseMove(object sender, MouseEventArgs e)
        {
            if (isInDrag)
            {
                var element = sender as FrameworkElement;
                currentPoint = e.GetPosition(null);

                transform.X += currentPoint.X - anchorPoint.X;
                transform.Y += (currentPoint.Y - anchorPoint.Y);
                this.RenderTransform = transform;
                anchorPoint = currentPoint;
            }
        }

        private void Lbdrag_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (isInDrag)
            {
                var element = sender as FrameworkElement;
                element.ReleaseMouseCapture();
                isInDrag = false;
                e.Handled = true;
            }
        }

    }
}
