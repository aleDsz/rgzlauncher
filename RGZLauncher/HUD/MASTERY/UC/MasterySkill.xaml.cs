﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CommonFeature.Const;
using RGZLauncher.GameLogic.OBJ;

namespace RGZLauncher.HUD.MASTERY.UC
{
    /// <summary>
    /// Interaction logic for MasterySkill.xaml
    /// </summary>
    public partial class MasterySkill : UserControl
    {
        private Mastery mastery;
        private int level;
        private int cost = 0;

        public MasterySkill()
        {
            InitializeComponent();
        }

        Dictionary<E_CURSOR, Cursor> Cursors = new Dictionary<E_CURSOR, Cursor>();

        public MasterySkill(Mastery mastery, int level)
        {
            InitializeComponent();

            Cursors.Add(E_CURSOR.NORMAL, new Cursor(new System.IO.MemoryStream(RGZLauncher.Properties.Resources.RagCur)));
            Cursors.Add(E_CURSOR.CLICK, new Cursor(new System.IO.MemoryStream(RGZLauncher.Properties.Resources.RagCurSel)));

            this.Cursor = Cursors[E_CURSOR.NORMAL];
            this.btUpSkill.Cursor = Cursors[E_CURSOR.CLICK];

            this.mastery = mastery;
            this.level = level;

            skill_levelMaximo.Content = "/" + mastery.max_level.ToString().PadLeft(3, '0');
            skill_levelAtual.Content = level.ToString().PadLeft(3, '0');
            skill_icon.Source = new BitmapImage(new Uri(CoreSingleton.Instance.ResourcesPath[CommonFeature.Const.E_RESOURCES.MasteryImages] + mastery.id.ToString() + ".png"));

            skill_name.Content = mastery.nome;
            skill_descript.Content = mastery.descricao;

            skill_descript.ToolTip = mastery.descricao;

            cost = level / 8;

            if (cost <= 0)
                cost = 1;
            else if (cost > 20)
                cost = 20;

            if (mastery.type == (int)E_MASTERYCURRENCY.EVENT)
                skill_cost.Content = "Pontos Necessários: " + (cost);
            else if (mastery.type == (int)E_MASTERYCURRENCY.CONNECTION)
                skill_cost.Content = "Pontos Necessários: " + (cost);
            else
                skill_cost.Content = "";
        }

        private void btUpSkill_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MasteryWindow MW = (MasteryWindow)CoreSingleton.Instance.GameUC[E_VIEW.E_MASTERYSCREEN];

            if (level >= mastery.max_level)
                return;

            bool bCanBuy = false;

            bCanBuy = CoreSingleton.Instance.player.points[mastery.type] >= cost;

            if (!bCanBuy)
            {
                MW.txPoints.Foreground = Brushes.Red;
                return;
            }

            MW.txPoints.Foreground = Brushes.Black;


            switch ((E_MASTERYCURRENCY)mastery.type)
            {
                case E_MASTERYCURRENCY.CONNECTION:
                    MW.txPoints.Content = "Pontos de Fidelidade: " + CoreSingleton.Instance.player.points[mastery.type];
                    break;

                case E_MASTERYCURRENCY.INSTANCE:
                    MW.txPoints.Content = "Não há pontos disponíveis";
                    break;

                case E_MASTERYCURRENCY.EVENT:
                    MW.txPoints.Content = "Pontos de Evento: " + CoreSingleton.Instance.player.points[mastery.type];
                    break;
            }

            level++;

            skill_levelAtual.Content = level.ToString().PadLeft(3, '0');
            skill_levelAtual.Foreground = Brushes.ForestGreen;

            skill_descript.Foreground = Brushes.ForestGreen;
            skill_cost.Foreground = Brushes.ForestGreen;

            if (MW.myChanges.ContainsKey(mastery.id))
                MW.myChanges[mastery.id] = level;
            else
                MW.myChanges.Add(mastery.id, level);

            MW.bCanOk = true;
            MW.btOk.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/GlobalTexture/BT_OK.png"));

            MW.player.points[mastery.type] -= cost;

            MW.updatePoints((E_MASTERYCURRENCY)mastery.type);

            cost = level / 8;

            if (cost <= 0)
                cost = 1;
            else if (cost > 20)
                cost = 20;

            skill_cost.Content = "Pontos Necessários: " + (cost);

            if ((CoreSingleton.Instance.player.points[mastery.type] - cost) < 0)
            {
                //MW.bCanOk = false;
                //MW.btOk.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/GlobalTexture/BT_OK_DISABLED.png"));
                skill_levelAtual.Foreground = Brushes.Red;
                btUpSkill.IsEnabled = false;
                MW.myChanges[mastery.id] = level - 1;
            }
        }

        private void btUpSkill_MouseEnter(object sender, MouseEventArgs e)
        {
            btUpSkill.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/MASTERY/Texture/MASTERY_UP_MOUSEON.png"));
        }

        private void btUpSkill_MouseLeave(object sender, MouseEventArgs e)
        {
            btUpSkill.Source = new BitmapImage(new Uri(@"pack://application:,,,/RGZLauncher;component/HUD/MASTERY/Texture/MASTERY_UP_MOUSEOFF.png"));
        }

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hwnd);
        private void Skill_icon_MouseEnter(object sender, MouseEventArgs e)
        {
            SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);
        }

        private void MainGrid_MouseLeave(object sender, MouseEventArgs e)
        {
            SetForegroundWindow(CoreSingleton.Instance.Client.Game.MainWindowHandle);
        }
    }
}
