﻿using CommonFeature.Const;
using Ionic.Zip;
using RGZLauncher.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using YamlDotNet.Core;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;

namespace RGZLauncher.LoginScreen
{
    public class Patcher
    {
        List<Update> UpdList = new List<Update>();

        int restartAfterFinish = 0;
        int restartWithUpdTool = 0;

        int atualDownload = 0;
        bool bFinishedDownload = true;

        private List<UpdFile> UpdFileList = new List<UpdFile>();

        bool downloadDone = false;

        MainWindow LS;

        public void donwloadFile()
        {
            downloadDone = false;
            
            if (UpdFileList.Count == 0 || atualDownload == UpdFileList.Count)
            {
                Settings.Default.CLIENT_VERSION = last_stringnum;
                Settings.Default.PATCH_VERSION = last_vernum;
                Settings.Default.Save();
            
                if (restartWithUpdTool > 0)
                {
                    Process.Start(new ProcessStartInfo(Directory.GetCurrentDirectory() + @"\SelfUpdater.exe"));
                    CoreSingleton.Instance.Finish();
                    return;
                }
                else
                {
                    LS.SetPB(100, "Finalizado");
                }
                return;
            }
            
            atualDownload++;
            Task<bool> downloadArchiveTask = DownloadArchiveAsync(UpdFileList[atualDownload - 1]);
        }

        async Task IsDownloadDone()
        {
            await Task.Run(() =>
            {
                while (!downloadDone) ;
            });
            donwloadFile();
        }

        int last_vernum = Settings.Default.PATCH_VERSION;
        string last_stringnum = Settings.Default.CLIENT_VERSION;


        public async Task<bool> DoUpdateLogic(MainWindow MW)
        {
            this.LS = MW;

            string yaml = (new WebClient()).DownloadString(new Uri(E_WEB.PATCH_FILE));

            TextReader db = new StringReader(yaml);

            var deserializer = new DeserializerBuilder().Build();

            var parser = new Parser(db);

            parser.Expect<StreamStart>();

            while (parser.Accept<DocumentStart>())
            {
                var doc = deserializer.Deserialize<List<Update>>(parser);

                foreach (var upd in doc)
                {
                    if (Settings.Default.PATCH_VERSION >= upd.NUM) continue;

                    if (upd.SELFUPDATE)
                        restartWithUpdTool++;

                    last_stringnum = upd.VERSION;
                    last_vernum = upd.NUM;

                    foreach (var item in upd.FILES)
                    {
                        UpdFileList.Add(item);
                    }
                }
            }

            donwloadFile();
            return restartWithUpdTool > 0;
        }

        async Task<bool> DownloadArchiveAsync(UpdFile upd)
        {
            var UriPath = new Uri(E_WEB.FILES_URL_BASE + upd.folder + "\\" + upd.name);

            using (WebClient webClient = new WebClient())
            {
                webClient.DownloadFileCompleted += (sender, args) =>
                {
                    LS.SetPB(100, "Extraindo " + UpdFileList[atualDownload - 1].name);

                    using (ZipFile zip1 = ZipFile.Read(Directory.GetCurrentDirectory() + "\\" + upd.name))
                    {
                        foreach (ZipEntry es in zip1)
                        {
                            es.Extract(Directory.GetCurrentDirectory(), ExtractExistingFileAction.OverwriteSilently);
                        }
                    }

                    File.Delete(Directory.GetCurrentDirectory() + "\\" + upd.name);

                    downloadDone = true;
                };

                webClient.DownloadProgressChanged += (sender, args) =>
                {
                    var downloadProgress = string.Format("{0} MB / {1} MB",
                        (args.BytesReceived / 1024d / 1024d).ToString("0.00"),
                        (args.TotalBytesToReceive / 1024d / 1024d).ToString("0.00"));

                    string dp = string.Format("Download {0}/{1} - {2}", atualDownload, UpdFileList.Count, downloadProgress);

                    LS.SetPB(args.ProgressPercentage > 100 ? 100 : args.ProgressPercentage, dp);
                };

                webClient.DownloadFileAsync(UriPath, Directory.GetCurrentDirectory() + "\\" + upd.name);
            }

            await IsDownloadDone();
            return true;
        }

        public class Update
        {
            public int NUM { get; set; }
            public string VERSION { get; set; }
            public bool SELFUPDATE { get; set; }
            public List<UpdFile> FILES { get; set; }
        }

        public class UpdFile
        {
            public string name { get; set; }
            public string folder { get; set; }
        }
    }
}

