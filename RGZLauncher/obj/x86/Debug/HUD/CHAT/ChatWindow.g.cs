﻿#pragma checksum "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "A0B2056C9ED7FDEEAC659E5C8CC7706BC677D8E4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using ExtendedRichTextBoxLibNuGet;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using RGZLauncher.HUD.CHAT;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace RGZLauncher.HUD.CHAT {
    
    
    /// <summary>
    /// ChatWindow
    /// </summary>
    public partial class ChatWindow : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 44 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl dialogtab;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ExtendedRichTextBoxLibNuGet.ExtendedRichTextBox chatbox;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ExtendedRichTextBoxLibNuGet.ExtendedRichTextBox grupobox;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ExtendedRichTextBoxLibNuGet.ExtendedRichTextBox guildbox;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem tabGlobal;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ExtendedRichTextBoxLibNuGet.ExtendedRichTextBox serverbox;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dialogbox;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox msg;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox whisper;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.PackIcon resizebutton;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.PackIcon sticker;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.PopupBox stickPopup;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox stickList;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.Badged mainToRead;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.Badged partyToRead;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.Badged guildToRead;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.Badged globalToRead;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/RGZLauncher;component/hud/chat/chatwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 10 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            ((RGZLauncher.HUD.CHAT.ChatWindow)(target)).MouseEnter += new System.Windows.Input.MouseEventHandler(this.UserControl_MouseEnter);
            
            #line default
            #line hidden
            
            #line 10 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            ((RGZLauncher.HUD.CHAT.ChatWindow)(target)).MouseLeave += new System.Windows.Input.MouseEventHandler(this.UserControl_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 2:
            this.dialogtab = ((System.Windows.Controls.TabControl)(target));
            
            #line 44 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            this.dialogtab.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Dialogtab_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.chatbox = ((ExtendedRichTextBoxLibNuGet.ExtendedRichTextBox)(target));
            return;
            case 4:
            this.grupobox = ((ExtendedRichTextBoxLibNuGet.ExtendedRichTextBox)(target));
            return;
            case 5:
            this.guildbox = ((ExtendedRichTextBoxLibNuGet.ExtendedRichTextBox)(target));
            return;
            case 6:
            this.tabGlobal = ((System.Windows.Controls.TabItem)(target));
            return;
            case 7:
            this.serverbox = ((ExtendedRichTextBoxLibNuGet.ExtendedRichTextBox)(target));
            return;
            case 8:
            this.dialogbox = ((System.Windows.Controls.Border)(target));
            
            #line 90 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            this.dialogbox.MouseMove += new System.Windows.Input.MouseEventHandler(this.Dialogbox_MouseMove);
            
            #line default
            #line hidden
            
            #line 90 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            this.dialogbox.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.Dialogbox_MouseLeftButtonUp);
            
            #line default
            #line hidden
            
            #line 90 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            this.dialogbox.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Dialogbox_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 9:
            this.msg = ((System.Windows.Controls.TextBox)(target));
            
            #line 96 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            this.msg.KeyDown += new System.Windows.Input.KeyEventHandler(this.TextBox_KeyDown);
            
            #line default
            #line hidden
            return;
            case 10:
            this.whisper = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.resizebutton = ((MaterialDesignThemes.Wpf.PackIcon)(target));
            
            #line 99 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            this.resizebutton.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.Resizebutton_MouseDown);
            
            #line default
            #line hidden
            return;
            case 12:
            this.sticker = ((MaterialDesignThemes.Wpf.PackIcon)(target));
            
            #line 100 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            this.sticker.MouseRightButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Sticker_MouseDown);
            
            #line default
            #line hidden
            return;
            case 13:
            this.stickPopup = ((MaterialDesignThemes.Wpf.PopupBox)(target));
            return;
            case 14:
            this.stickList = ((System.Windows.Controls.ListBox)(target));
            return;
            case 16:
            this.mainToRead = ((MaterialDesignThemes.Wpf.Badged)(target));
            
            #line 124 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            this.mainToRead.BadgeChanged += new System.Windows.RoutedPropertyChangedEventHandler<object>(this.BadgeChanged);
            
            #line default
            #line hidden
            return;
            case 17:
            this.partyToRead = ((MaterialDesignThemes.Wpf.Badged)(target));
            
            #line 131 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            this.partyToRead.BadgeChanged += new System.Windows.RoutedPropertyChangedEventHandler<object>(this.BadgeChanged);
            
            #line default
            #line hidden
            return;
            case 18:
            this.guildToRead = ((MaterialDesignThemes.Wpf.Badged)(target));
            
            #line 138 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            this.guildToRead.BadgeChanged += new System.Windows.RoutedPropertyChangedEventHandler<object>(this.BadgeChanged);
            
            #line default
            #line hidden
            return;
            case 19:
            this.globalToRead = ((MaterialDesignThemes.Wpf.Badged)(target));
            
            #line 145 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            this.globalToRead.BadgeChanged += new System.Windows.RoutedPropertyChangedEventHandler<object>(this.BadgeChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            System.Windows.EventSetter eventSetter;
            switch (connectionId)
            {
            case 15:
            eventSetter = new System.Windows.EventSetter();
            eventSetter.Event = System.Windows.UIElement.PreviewMouseLeftButtonDownEvent;
            
            #line 107 "..\..\..\..\..\HUD\CHAT\ChatWindow.xaml"
            eventSetter.Handler = new System.Windows.Input.MouseButtonEventHandler(this.lvStick_sendSticker);
            
            #line default
            #line hidden
            ((System.Windows.Style)(target)).Setters.Add(eventSetter);
            break;
            }
        }
    }
}

