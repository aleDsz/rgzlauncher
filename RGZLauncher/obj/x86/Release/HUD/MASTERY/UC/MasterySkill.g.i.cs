﻿#pragma checksum "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "447D7F8C087A0A0C4C66B8FE6754B7EB12F8D0EE"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using RGZLauncher.HUD.MASTERY.UC;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace RGZLauncher.HUD.MASTERY.UC {
    
    
    /// <summary>
    /// MasterySkill
    /// </summary>
    public partial class MasterySkill : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image skill_icon;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label skill_name;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label skill_cost;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label skill_levelAtual;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label skill_levelMaximo;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image btUpSkill;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label skill_descript;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/RGZLauncher;component/hud/mastery/uc/masteryskill.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.skill_icon = ((System.Windows.Controls.Image)(target));
            
            #line 12 "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml"
            this.skill_icon.MouseEnter += new System.Windows.Input.MouseEventHandler(this.Skill_icon_MouseEnter);
            
            #line default
            #line hidden
            return;
            case 2:
            this.skill_name = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.skill_cost = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.skill_levelAtual = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.skill_levelMaximo = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.btUpSkill = ((System.Windows.Controls.Image)(target));
            
            #line 25 "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml"
            this.btUpSkill.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btUpSkill_MouseDown);
            
            #line default
            #line hidden
            
            #line 25 "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml"
            this.btUpSkill.MouseEnter += new System.Windows.Input.MouseEventHandler(this.btUpSkill_MouseEnter);
            
            #line default
            #line hidden
            
            #line 25 "..\..\..\..\..\..\HUD\MASTERY\UC\MasterySkill.xaml"
            this.btUpSkill.MouseLeave += new System.Windows.Input.MouseEventHandler(this.btUpSkill_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 7:
            this.skill_descript = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

