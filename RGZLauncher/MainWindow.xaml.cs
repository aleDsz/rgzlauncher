﻿using CommonFeature.Const;
using Microsoft.Win32;
using RGZLauncher.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RGZLauncher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool bCanPlay = true;

        public MainWindow()
        {
            CoreSingleton.Instance.Views.Add(E_VIEW.E_SPLASHSCREEN, this);
            CoreSingleton.Instance.Init(E_INIT.Basic, new int[] { 0 });
            CoreSingleton.Instance.Init(E_INIT.Language, new int[] { (int)E_Language.English });
            CoreSingleton.Instance.Init(E_INIT.Patcher, new int[] { 0 });

            InitializeComponent();

            btStart.Visibility = Visibility.Hidden;

            var browser = new WebBrowser();
            Registry.SetValue(
            @"HKEY_CURRENT_USER\SOFTWARE\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BROWSER_EMULATION",
            AppDomain.CurrentDomain.FriendlyName,
            int.MaxValue);
            browser.Navigate("http://ragnaghostz.com.br/patch/news/newsfb.php");

            if(!string.IsNullOrEmpty(Settings.Default.USERNAME))
            {
                txLogin.Text = Settings.Default.USERNAME;
                txPassword.Password = Settings.Default.PASSWORD;
                ckSalvarCred.IsChecked = true;
            }

            E_INIT[] inits = { E_INIT.Basic, E_INIT.Discord, E_INIT.Language, E_INIT.Megumi, E_INIT.Monsters, E_INIT.Mastery };

            Dictionary<E_INIT, int[]> parameters = new Dictionary<E_INIT, int[]>();

            parameters.Add(E_INIT.Basic, new int[] { 0 }); // Basic
            parameters.Add(E_INIT.Discord, new int[] { 0 }); // Discord
            parameters.Add(E_INIT.Language, new int[] { (int)E_Language.English }); // Language
            parameters.Add(E_INIT.Megumi, new int[] { 0 }); // Megumi
            parameters.Add(E_INIT.Monsters, new int[] { 0 }); // Monster
            parameters.Add(E_INIT.Mastery, new int[] { 0 }); // Maestrias

            foreach (E_INIT s in inits)
            {
                CoreSingleton.Instance.Init(s, parameters[s]);

                //Thread.Sleep(1000);
            }
        }

        private void BtStart_Click(object sender, RoutedEventArgs e)
        {
            if(txLogin.Text.Length < 4 || txLogin.Text.Length > 25)
            {
                MessageBox.Show("O login deve conter ao menos 4 e no máximo 25 caracteres.");
                return;
            }

            if(txPassword.Password.Length < 4 || txPassword.Password.Length > 40)
            {
                MessageBox.Show("A senha deve conter ao menos 4 caracteres.");
                return;
            }

            Settings.Default.USERNAME = ckSalvarCred.IsChecked.Value ? txLogin.Text : "";
            Settings.Default.PASSWORD = ckSalvarCred.IsChecked.Value ? txPassword.Password : "";

            Settings.Default.Save();

            StreamReader sr = new StreamReader(Directory.GetCurrentDirectory() + @"\savedata\OptionInfo.lua");

            string luaLine = sr.ReadLine();

            string lua = "";

            while (luaLine != null)
            {
                if (luaLine.Contains("[\"ISFULLSCREENMODE\"] = 1") || luaLine.Contains("[\"MouseExclusive\"] = 1"))
                    luaLine = luaLine.Replace("1", "0");

                lua += luaLine + "\n";

                luaLine = sr.ReadLine();
            }

            sr.Close();

            using (FileStream fs = File.Create(Directory.GetCurrentDirectory() + @"\savedata\OptionInfo.lua"))
            {
                // Add some text to file    
                Byte[] content = new UTF8Encoding(true).GetBytes(lua);
                fs.Write(content, 0, content.Length);
            }

            CoreSingleton.Instance.StartGame(@"\RagnaGhostz.exe", txLogin.Text, txPassword.Password);
            this.Close();
        }

        public void SetPB(double val, string whoIsLoading)
        {
            Application.Current.Dispatcher.InvokeAsync(new Action(() =>
            {
                pbUpdate.Value = val;
                lbUpdProgress.Content = whoIsLoading;
                //lbLoading.Content = whoIsLoading;
                //Console.WriteLine(val);

                if (whoIsLoading.Equals("Finalizado"))
                {
                    btStart.Visibility = Visibility.Visible;
                    this.Focus();
                }
            }));
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            new Task(async () =>
            {
                Task.Delay(5000);
                bool b = await CoreSingleton.Instance.Patch.DoUpdateLogic(this);
            }).Start();
        }

        private void BtClose_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            Environment.Exit(0);
        }

        private void BtSetup_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(Directory.GetCurrentDirectory() + @"\opensetup.exe");
        }

        private void BtZerarPatch_Click(object sender, RoutedEventArgs e)
        {
            Settings.Default.PATCH_VERSION = 0;
            Settings.Default.Save();
            new Task(async () =>
            {
                Task.Delay(5000);
                bool b = await CoreSingleton.Instance.Patch.DoUpdateLogic(this);
            }).Start();
        }

        private void BGVideo_Loaded(object sender, RoutedEventArgs e)
        {
            Uri videoUri = new Uri(Directory.GetCurrentDirectory() + "/RGZ/rgzwallpaper.mp4");

            BGVideo.Source = videoUri;
            BGVideo.Volume = 0.0;
            BGVideo.LoadedBehavior = MediaState.Manual;
            BGVideo.Play();
        }

        private void BGVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            BGVideo.Position = TimeSpan.FromSeconds(0);
            BGVideo.Play();
        }

        private void BtDiscord_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://discord.gg/6VGd5dV");
        }

        private void BtRegister_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://ragnaghostz.com.br/site/?module=account&action=create");
        }
    }
}
