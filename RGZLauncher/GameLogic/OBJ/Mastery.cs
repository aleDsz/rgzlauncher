﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGZLauncher.GameLogic.OBJ
{
    public class Mastery
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public int type { get; set; }
        public int job { get; set; }
        public int max_level { get; set; }

        // Player
        public int actual_level { get; set; } = 0;
    }
}
