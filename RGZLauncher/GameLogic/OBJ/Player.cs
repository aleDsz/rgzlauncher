﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGZLauncher.GameLogic.OBJ
{
    public class Player
    {
        public State state = new State();
        public Dictionary<int, int> masteries = new Dictionary<int, int>();

        // 0 - Zeny
        // 1 - Evento
        // 2 - Instancias
        public int[] points = new int[3];

        public void AddMastery(int id, int level)
        {
            if (masteries.ContainsKey(id))
            {
                masteries[id] = level;
                return;
            }

            masteries.Add(id, level);
        }

        public void CleanPlayer()
        {
            masteries.Clear();
            state = new State();
        }
    }

    public class State
    {
        public bool dressroom;
        public int[] dressoption = new int[3] { 0, 0, 0 };

        public bool mastery;
        public bool firstWalk;
    }
}
