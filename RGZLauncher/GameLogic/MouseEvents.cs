﻿using Gma.System.MouseKeyHook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RGZLauncher.GameLogic
{
    public class MouseEvents
    {
        public IKeyboardMouseEvents MouseHook;

        private bool MouseIsDown = true;

        private Thread MouseDownThread;

        public MouseEvents()
        {
            MouseHook = Hook.GlobalEvents();

            MouseHook.MouseDown += OnMouseDown;
            MouseHook.MouseUp += OnMouseUp;
        }

        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) return;

            MouseIsDown = false;

            if (MouseDownThread != null)
                MouseDownThread.Abort();
        }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) return;
        }
    }
}

