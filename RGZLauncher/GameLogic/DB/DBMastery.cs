﻿using RGZLauncher.GameLogic.OBJ;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Core;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;

namespace RGZLauncher.GameLogic.DB
{
    public class DBMastery
    {
        public Dictionary<int, Mastery> Masteries = new Dictionary<int, Mastery>();

        public DBMastery(string path)
        {
            TextReader db = new StringReader(File.ReadAllText(path + "mastery.yml", Encoding.UTF8));

            var deserializer = new DeserializerBuilder().Build();

            var parser = new Parser(db);

            parser.Expect<StreamStart>();

            while (parser.Accept<DocumentStart>())
            {
                var doc = deserializer.Deserialize<List<Mastery>>(parser);

                int c = 0;

                foreach (var item in doc)
                {
                    Masteries.Add(item.id, item);
                }
            }
        }
    }
}
