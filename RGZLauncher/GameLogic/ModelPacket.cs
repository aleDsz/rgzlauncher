﻿using CommonFeature.Const;
using RGZLauncher.HUD.CHAT;
using RGZLauncher.HUD.MAIN;
using RGZLauncher.HUD.MASTERY;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;

namespace RGZLauncher.GameLogic
{
    public static class DispatcherUtil
    {

        public static void AsyncWorkAndUIThreadUpdate<T>(this Dispatcher currentDispatcher, Func<T> threadWork, Action<T> guiUpdate)
        {
            ThreadPool.QueueUserWorkItem(delegate (object state)
            {
                T resultAfterThreadWork = threadWork();
                currentDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<T>(delegate (T result) {
                    guiUpdate(resultAfterThreadWork);
                }), resultAfterThreadWork);

            });
        }

    }

    public class ModelPacket
    {
        public void ParserPacket(string packet)
        {
            string[] pk = packet.Split(':');

            if (!pk[0].Equals("CMD")) return;

            string[] data = pk[2].Split('|');
            int[] extra_int = new int[8];

            // Alguns packets possuem : como o do chat, então fazemos essa gambiarra aqui
            string all_data = packet.Replace("CMD:", "").Replace(pk[1] + ":","");

            switch (pk[1])
            {
                case "CLEARPLAYERDATA":
                    CoreSingleton.Instance.player.CleanPlayer();

                    switch((E_CLEARDATA)Int32.Parse(data[0]))
                    {
                        case E_CLEARDATA.MASTERY:
                            Application.Current.Dispatcher.Invoke(new Action(() =>
                            {
                                CoreSingleton.Instance.player.masteries.Clear();

                                if (!CoreSingleton.Instance.GameUC.ContainsKey(CommonFeature.Const.E_VIEW.E_MASTERYSCREEN))
                                {
                                    MasteryWindow MW = new MasteryWindow();

                                    CoreSingleton.Instance.GameUC.Add(CommonFeature.Const.E_VIEW.E_MASTERYSCREEN, MW);
                                    ((MainHud)CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).maingrid.Children.Add(MW);
                                }
                            }));
                            break;

                        case E_CLEARDATA.CHAT:
                            if (!CoreSingleton.Instance.GameUC.ContainsKey(CommonFeature.Const.E_VIEW.E_CHATSCREEN))
                            {
                                Application.Current.Dispatcher.Invoke(new Action(() =>
                                {
                                    //if (CoreSingleton.Instance.GameUC.ContainsKey(CommonFeature.Const.E_VIEW.E_CHATSCREEN))
                                    //{
                                    //    ((MainHud)CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).maingrid.Children.Remove(CoreSingleton.Instance.GameUC[E_VIEW.E_CHATSCREEN]);
                                    //    CoreSingleton.Instance.GameUC.Remove(CommonFeature.Const.E_VIEW.E_CHATSCREEN);
                                    //}

                                    ChatWindow CW = new ChatWindow();

                                    CoreSingleton.Instance.GameUC.Add(CommonFeature.Const.E_VIEW.E_CHATSCREEN, CW);

                                    ((MainHud)CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).maingrid.Children.Add(CW);
                                }));
                            }

                            if (CoreSingleton.Instance.GameUC.ContainsKey(CommonFeature.Const.E_VIEW.E_CHATSCREEN))
                            {
                                Application.Current.Dispatcher.Invoke(new Action(() =>
                                {
                                    //if (CoreSingleton.Instance.GameUC.ContainsKey(CommonFeature.Const.E_VIEW.E_CHATSCREEN))
                                    //{
                                    //    ((MainHud)CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).maingrid.Children.Remove(CoreSingleton.Instance.GameUC[E_VIEW.E_CHATSCREEN]);
                                    //    CoreSingleton.Instance.GameUC.Remove(CommonFeature.Const.E_VIEW.E_CHATSCREEN);
                                    //}

                                    ((ChatWindow)CoreSingleton.Instance.GameUC[E_VIEW.E_CHATSCREEN]).clearChats();

                                }));
                            }
                            break;
                    }

                    break;

                case "DISPPARTY":
                case "DISPGUILD":
                case "DISPSERVER":
                case "DISP":
                    {
                        if (!CoreSingleton.Instance.GameUC.ContainsKey(CommonFeature.Const.E_VIEW.E_CHATSCREEN))
                        {
                            Application.Current.Dispatcher.Invoke(new Action(() =>
                            {
                            //if (CoreSingleton.Instance.GameUC.ContainsKey(CommonFeature.Const.E_VIEW.E_CHATSCREEN))
                            //{
                            //    ((MainHud)CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).maingrid.Children.Remove(CoreSingleton.Instance.GameUC[E_VIEW.E_CHATSCREEN]);
                            //    CoreSingleton.Instance.GameUC.Remove(CommonFeature.Const.E_VIEW.E_CHATSCREEN);
                            //}

                            ChatWindow CW = new ChatWindow();

                                CoreSingleton.Instance.GameUC.Add(CommonFeature.Const.E_VIEW.E_CHATSCREEN, CW);

                                ((MainHud)CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).maingrid.Children.Add(CW);
                            }));
                        }

                        ((ChatWindow)CoreSingleton.Instance.GameUC[E_VIEW.E_CHATSCREEN]).addText(all_data, pk[1]);
                    }
                    break;

                case "ADDSTICKER":
                    if (!CoreSingleton.Instance.GameUC.ContainsKey(CommonFeature.Const.E_VIEW.E_CHATSCREEN))
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            //if (CoreSingleton.Instance.GameUC.ContainsKey(CommonFeature.Const.E_VIEW.E_CHATSCREEN))
                            //{
                            //    ((MainHud)CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).maingrid.Children.Remove(CoreSingleton.Instance.GameUC[E_VIEW.E_CHATSCREEN]);
                            //    CoreSingleton.Instance.GameUC.Remove(CommonFeature.Const.E_VIEW.E_CHATSCREEN);
                            //}

                            ChatWindow CW = new ChatWindow();

                            CoreSingleton.Instance.GameUC.Add(CommonFeature.Const.E_VIEW.E_CHATSCREEN, CW);

                            ((MainHud)CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).maingrid.Children.Add(CW);
                        }));
                    }

                    ((ChatWindow)CoreSingleton.Instance.GameUC[E_VIEW.E_CHATSCREEN]).enableSticker(data[0]);
                    break;

                case "STICKER":
                    if (!CoreSingleton.Instance.GameUC.ContainsKey(CommonFeature.Const.E_VIEW.E_CHATSCREEN))
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            //if (CoreSingleton.Instance.GameUC.ContainsKey(CommonFeature.Const.E_VIEW.E_CHATSCREEN))
                            //{
                            //    ((MainHud)CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).maingrid.Children.Remove(CoreSingleton.Instance.GameUC[E_VIEW.E_CHATSCREEN]);
                            //    CoreSingleton.Instance.GameUC.Remove(CommonFeature.Const.E_VIEW.E_CHATSCREEN);
                            //}

                            ChatWindow CW = new ChatWindow();

                            CoreSingleton.Instance.GameUC.Add(CommonFeature.Const.E_VIEW.E_CHATSCREEN, CW);

                            ((MainHud)CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).maingrid.Children.Add(CW);
                        }));
                    }

                    ((ChatWindow)CoreSingleton.Instance.GameUC[E_VIEW.E_CHATSCREEN]).addSticker(data[0]);
                    break;

                case "STOPSOUND":
                    if (CoreSingleton.Instance.audioFile != null)
                    {
                        CoreSingleton.Instance.NoLoop = true;
                        CoreSingleton.Instance.outputDevice.Dispose();
                        CoreSingleton.Instance.NoLoop = false;
                        CoreSingleton.Instance.outputDevice = new NAudio.Wave.WaveOutEvent();
                    }
                    break;

                case "PLAYSOUND":
                    if (CoreSingleton.Instance.audioFile != null)
                    {
                        CoreSingleton.Instance.NoLoop = true;
                        CoreSingleton.Instance.outputDevice.Dispose();
                        CoreSingleton.Instance.NoLoop = false;
                        CoreSingleton.Instance.outputDevice = new NAudio.Wave.WaveOutEvent();
                    }

                    CoreSingleton.Instance.audioFile = new NAudio.Wave.AudioFileReader(CoreSingleton.Instance.ResourcesPath[E_RESOURCES.Sound] + data[0]);
                    CoreSingleton.Instance.NoLoop = true;
                    CoreSingleton.Instance.outputDevice.Stop();
                    CoreSingleton.Instance.outputDevice.Init(CoreSingleton.Instance.audioFile);
                    CoreSingleton.Instance.outputDevice.Play();
                    CoreSingleton.Instance.NoLoop = false;
                    break;

                case "DRESSROOM":
                    //MessageBox.Show("Aberto a DressRoom! " + Int32.Parse(data[0]));
                    CoreSingleton.Instance.player.state.dressroom = true;
                    break;

                case "SENDSTYLE":
                    if (!CoreSingleton.Instance.player.state.dressroom) return;

                    CoreSingleton.Instance.player.state.dressoption[0] = CoreSingleton.Instance.RagHook.GetSelectedInStyleUI(CommonFeature.Const.E_CHARSPRITE.HAIR);
                    CoreSingleton.Instance.player.state.dressoption[1] = CoreSingleton.Instance.RagHook.GetSelectedInStyleUI(CommonFeature.Const.E_CHARSPRITE.HAIR_COLOR);
                    CoreSingleton.Instance.player.state.dressoption[2] = CoreSingleton.Instance.RagHook.GetSelectedInStyleUI(CommonFeature.Const.E_CHARSPRITE.BODY_COLOR);

                    CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_STYLE(CoreSingleton.Instance.player.state.dressoption);
                    CoreSingleton.Instance.player.state.dressroom = false;
                    break;

                case "RPC":                     
                    CoreSingleton.Instance.Discord.UpdatePresenceSimple(data[0], data[1], data[2], data[3], data[4], data[5]);
                    break;

                case "MASTERY":
                    CoreSingleton.Instance.player.AddMastery(Int32.Parse(data[0]), Int32.Parse(data[1]));
                    break;

                case "REQMAC":
                    CoreSingleton.Instance.MegumiControl.GetPacketSender().PK_MAC(GetDefaultMacAddress());
                    break;

                case "OPENMASTERY":
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            CoreSingleton.Instance.player.state.mastery = true;

                            CoreSingleton.Instance.player.points[0] = int.Parse(data[0]);
                            CoreSingleton.Instance.player.points[1] = int.Parse(data[1]);
                            CoreSingleton.Instance.player.points[2] = int.Parse(data[2]);

                            ((MasteryWindow)CoreSingleton.Instance.GameUC[E_VIEW.E_MASTERYSCREEN]).Load(CoreSingleton.Instance.player);

                            SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);
                            SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);
                            SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);
                        }));
                    }
                    break;
            }
        }

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hwnd);

        public string GetDefaultMacAddress()
        {
            Dictionary<string, long> macAddresses = new Dictionary<string, long>();
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                    macAddresses[nic.GetPhysicalAddress().ToString()] = nic.GetIPStatistics().BytesSent + nic.GetIPStatistics().BytesReceived;
            }
            long maxValue = 0;
            string mac = "";
            foreach (KeyValuePair<string, long> pair in macAddresses)
            {
                if (pair.Value > maxValue)
                {
                    mac = pair.Key;
                    maxValue = pair.Value;
                }
            }
            return mac;
        }
    }
}
