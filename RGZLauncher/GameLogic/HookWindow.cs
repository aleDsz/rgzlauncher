﻿using CommonFeature.Client;
using CommonFeature.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Threading;

namespace RGZLauncher.GameLogic
{
    /// <summary>
    /// Interaction logic for HookWindow.xaml
    /// </summary>
    public abstract class HookWindow : Window
    {
        Dictionary<E_CURSOR, Cursor> Cursors = new Dictionary<E_CURSOR, Cursor>();

        public HookWindow()
        {

        }

        public void MakeView(E_VIEW view)
        {
            Cursors.Add(E_CURSOR.NORMAL, new Cursor(new System.IO.MemoryStream(RGZLauncher.Properties.Resources.RagCur)));
            Cursors.Add(E_CURSOR.CLICK, new Cursor(new System.IO.MemoryStream(RGZLauncher.Properties.Resources.RagCurSel)));

            this.Cursor = Cursors[E_CURSOR.NORMAL];

            try
            {
                CoreSingleton.Instance.GameWindow.Add(view, this);
            }
            catch (Exception)
            {
            }

        }

        private static System.Timers.Timer aTimer;

        public void PositionTimer()
        {
            this.Height = CoreSingleton.Instance.Client.Client_Height + 55;
            this.Width = CoreSingleton.Instance.Client.Client_Widht + 6;

            aTimer = new System.Timers.Timer(50);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += Refresh;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;

            
        }

        public void MouseEnterCursor(object sender, MouseEventArgs e)
        {
            CoreSingleton.Instance.RagHook.MoveCursor(9999, 0);
        }

        RECT rect_old = new RECT();

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hwnd);

        public void Refresh(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(new System.Action(() =>
            {
                try
                {
                    CoreSingleton.Instance.RagHook.HideRagHUD();

                    if (CoreSingleton.Instance.Client.IsRunning())
                    {
                        IntPtr windowHandle = new WindowInteropHelper(this).Handle;
                        IntPtr hwnd = WinApi.GetForegroundWindow();

                        bool thisFocus = WinApi.ApplicationIsActivated();

                        string map = CoreSingleton.Instance.RagHook.GetStringValue(OffSets.MAP.NAME);

                        if ((hwnd != CoreSingleton.Instance.Client.Game.MainWindowHandle && !thisFocus) || map.Contains("login"))
                        {
                            Hide();
                            Topmost = false;
                        }
                        else
                        {
                            IntPtr w = new WindowInteropHelper(this).Handle;
                            WinApi.SetWindowPos(w, WinApi.HWND_TOPMOST, 0, 0, 0, 0, WinApi.TOPMOST_FLAGS);

                            Show();
                            var rect = new RECT();
                            var get = WinApi.GetWindowRect(CoreSingleton.Instance.Client.Game.MainWindowHandle, out rect);

                            rect_old = rect;

                            MoveWindow(w, (int)rect.left, (int)rect.top + 25, rect.right - rect.left, (rect.bottom - rect.top) - 25, true);

                            //SetForegroundWindow(new WindowInteropHelper(CoreSingleton.Instance.GameWindow[CommonFeature.Const.E_VIEW.E_MAINHUD]).Handle);

                            //foreach (KeyValuePair<E_VIEW, Window> view in CoreSingleton.Instance.GameWindow)
                            //{
                            //    if (view.Key == E_VIEW.E_MAINHUD) continue;
                            //
                            //    RECT Rect2 = new RECT();
                            //
                            //    var get2 = WinApi.GetWindowRect(CoreSingleton.Instance.Client.Game.MainWindowHandle, out Rect2);
                            //
                            //    IntPtr w2 = new WindowInteropHelper(view.Value).Handle;
                            //
                            //    view.Value.Left = this.Left + ((this.Width - view.Value.Width) / 2);
                            //    view.Value.Top = this.Top + ((this.Height - view.Value.Height) / 2);
                            //}

                            //CoreSingleton.Instance.RagHook.HideRagHUD();

                            Topmost = true;
                        }
                    }
                    else
                        this.Close();
                }
                catch (Exception ex)
                {
                    return;
                }
            }));
        }

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = true, SetLastError = true)]
        internal static extern bool GetWindowRect(IntPtr hWnd, ref RECT rect);

        private bool mouseDown;
        private Point lastLocation;

        private void TitleBar_MouseUp(object sender, MouseButtonEventArgs e)
        {
            mouseDown = false;
        }

        public abstract void RectLogic(RECT rect);
        public abstract void Border_MouseDown(object sender, MouseButtonEventArgs e);
        public abstract void Window_LocationChanged(object sender, EventArgs e);

        #region Task Bar Hack
        #region Window styles
        [Flags]
        public enum ExtendedWindowStyles
        {
            // ...
            WS_EX_TOOLWINDOW = 0x00000080,
            // ...
        }

        public enum GetWindowLongFields
        {
            // ...
            GWL_EXSTYLE = (-20),
            // ...
        }

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowLong(IntPtr hWnd, int nIndex);

        public static IntPtr SetWindowLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong)
        {
            int error = 0;
            IntPtr result = IntPtr.Zero;
            // Win32 SetWindowLong doesn't clear error on success
            SetLastError(0);

            if (IntPtr.Size == 4)
            {
                // use SetWindowLong
                Int32 tempResult = IntSetWindowLong(hWnd, nIndex, IntPtrToInt32(dwNewLong));
                error = Marshal.GetLastWin32Error();
                result = new IntPtr(tempResult);
            }
            else
            {
                // use SetWindowLongPtr
                result = IntSetWindowLongPtr(hWnd, nIndex, dwNewLong);
                error = Marshal.GetLastWin32Error();
            }

            if ((result == IntPtr.Zero) && (error != 0))
            {
                throw new System.ComponentModel.Win32Exception(error);
            }

            return result;
        }

        [DllImport("user32.dll", EntryPoint = "SetWindowLongPtr", SetLastError = true)]
        private static extern IntPtr IntSetWindowLongPtr(IntPtr hWnd, int nIndex, IntPtr dwNewLong);

        [DllImport("user32.dll", EntryPoint = "SetWindowLong", SetLastError = true)]
        private static extern Int32 IntSetWindowLong(IntPtr hWnd, int nIndex, Int32 dwNewLong);

        private static int IntPtrToInt32(IntPtr intPtr)
        {
            return unchecked((int)intPtr.ToInt64());
        }

        [DllImport("kernel32.dll", EntryPoint = "SetLastError")]
        public static extern void SetLastError(int dwErrorCode);

        public void TaskHack()
        {
            WindowInteropHelper wndHelper = new WindowInteropHelper(this);

            int exStyle = (int)GetWindowLong(wndHelper.Handle, (int)GetWindowLongFields.GWL_EXSTYLE);

            exStyle |= (int)ExtendedWindowStyles.WS_EX_TOOLWINDOW;
            SetWindowLong(wndHelper.Handle, (int)GetWindowLongFields.GWL_EXSTYLE, (IntPtr)exStyle);
        }
        #endregion
        #endregion
    }
}

