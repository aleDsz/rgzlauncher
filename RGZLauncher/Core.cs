﻿using CommonFeature.Client;
using CommonFeature.Const;
using NAudio.Wave;
using RGZLauncher.GameLogic;
using RGZLauncher.GameLogic.DB;
using RGZLauncher.HUD.MAIN;
using RGZLauncher.LoginScreen;
using RGZLauncher.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using static CommonFeature.Client.OffSets;

namespace RGZLauncher
{
    public sealed class CoreSingleton
    {
        static CoreSingleton _instance;

        public Dictionary<E_VIEW, Window> Views = new Dictionary<E_VIEW, Window>();
        public Dictionary<E_VIEW, Window> GameWindow = new Dictionary<E_VIEW, Window>();
        public Dictionary<E_VIEW, UserControl> GameUC = new Dictionary<E_VIEW, UserControl>();

        public Patcher Patch;
        public CommonFeature.Discord.DiscordHandler Discord;
        public CommonFeature.Client.Ragexe Client;
        public CommonFeature.Client.Hook RagHook;
        //public MouseEvents MouseEvents;

        public GameLogic.OBJ.Player player = new GameLogic.OBJ.Player();

        public Megumi.Megumi MegumiControl;
        public ModelPacket MVPacket;

        public Dictionary<E_RESOURCES, string> ResourcesPath;

        public WaveOutEvent outputDevice = new WaveOutEvent();
        public AudioFileReader audioFile;

        //public Popup PopUp_Ult;

        //DB
        //public DBMonster dbMonster;
        public DBMastery dbMastery;

        //Timers
        public Thread PacketThread;

        public static CoreSingleton Instance
        {
            get { return _instance ?? (_instance = new CoreSingleton()); }
        }

        public bool NoLoop { get; internal set; }

        private CoreSingleton()
        {
            outputDevice.PlaybackStopped += OnPlaybackStopped;
        }

        private void OnPlaybackStopped(object sender, StoppedEventArgs e)
        {
            audioFile.Position = 0;

            try
            {

                if (!NoLoop)
                    outputDevice.Play();
            }
            catch(Exception es)
            {

            }
        }

        public void Init(E_INIT init, int[] parameters)
        {
            switch (init)
            {
                case E_INIT.Basic:
                    InitBasic(Directory.GetCurrentDirectory());
                    break;

                case E_INIT.Language:
                    CommonFeature.Translator.TranslatorSingleton.Instance.SetUserLanguage(ResourcesPath[E_RESOURCES.DB], (E_Language)parameters[0]);
                    break;

                case E_INIT.Discord:
                    Discord = new CommonFeature.Discord.DiscordHandler("641084521978134531");
                    Discord.presence.startTimestamp = 0;
                    Discord.UpdatePresenceSimple("Tela de Login", "Quem vai encarar?", "default", "Tela de Login");
                    break;

                case E_INIT.Monsters:
                    //dbMonster = new DBMonster(ResourcesPath[E_RESOURCES.DB], TP);
                    break;

                case E_INIT.Mastery:
                    dbMastery = new DBMastery(ResourcesPath[E_RESOURCES.DB]);
                    break;

                case E_INIT.Megumi:
                    MVPacket = new ModelPacket();
                    break;

                case E_INIT.Patcher:
                    Patch = new Patcher();
                    break;
            }
        }

        public void InitBasic(string application_dir)
        {
            ResourcesPath = new Dictionary<E_RESOURCES, string>();
            ResourcesPath.Add(E_RESOURCES.DB, application_dir + @"\RGZ\DB\");
            ResourcesPath.Add(E_RESOURCES.MasteryImages, application_dir + @"\RGZ\Images\Mastery\");
            ResourcesPath.Add(E_RESOURCES.StickerImages, application_dir + @"\RGZ\Images\Sticker\");
            ResourcesPath.Add(E_RESOURCES.ChatIcons, application_dir + @"\RGZ\Images\Chat\");
            ResourcesPath.Add(E_RESOURCES.Sound, application_dir + @"\RGZ\Sounds\");
        }

        public void ReadPacketStack()
        {
            while (MegumiControl.GetPacketQueue().Count > 0)
            {
                string packet = "";

                if (MegumiControl.GetPacketQueue().TryDequeue(out packet))
                {
                    MVPacket.ParserPacket(packet);
                }
            }
        }

        public CancellationTokenSource _cancelationTokenSource = new CancellationTokenSource();

        public void StartGame(string executable, string username, string password)
        {
            MegumiControl = new Megumi.Megumi(username, password, true);

            PacketThread = new Thread(() =>
            {
                while (true)
                {
                    ReadPacketStack();
                    Thread.Sleep(100);
                }
            });

            Client = new CommonFeature.Client.Ragexe(executable, MegumiControl.GetSession());
            RagHook = new CommonFeature.Client.Hook(Client);

            while (!Client.IsRunning())
            {
                Thread.Sleep(3000);
            }

            HookWindow HW = new MainHud();
            HW.Show();
            HW.PositionTimer();

            //MouseEvents = new MouseEvents();

            PacketThread.Start();

            new Task(() =>
            {
                while (Client.IsRunning())
                {
                    Thread.Sleep(3000);
                }

                Finish();
            }).Start();
        }

        public void Finish()
        {
            if (Discord != null)
                Discord.ShutDown();

            //if (MouseEvents != null)
                //MouseEvents.MouseHook.Dispose();

            Settings.Default.Save();

            Environment.Exit(0);
        }

        //Sets a window to be a child window of another window
        [DllImport("USER32.DLL")]
        public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        //Sets window attributes
        [DllImport("USER32.DLL")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        //Gets window attributes
        [DllImport("USER32.DLL")]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

        [DllImport("user32.dll")]
        private static extern int ShowWindow(int hwnd, int command);

        //assorted constants needed

        //assorted constants needed
        public static int GWL_STYLE = -16;
        public static int WS_CHILD = 0x40000000; //child window
        public static int WS_BORDER = 0x00800000; //window with border
        public static int WS_DLGFRAME = 0x00400000; //window with double border but no title
        public static int WS_CAPTION = WS_BORDER | WS_DLGFRAME; //window with a title bar

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, UInt32 uFlags);
    }
}
